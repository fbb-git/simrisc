#define XERR
#include "survival.ih"

void Survival::varyLung(ostream &out)
{
    VSD::vary(out, 2, "Survival:", s_noColon[0], 'a', d_vsdMatrix[0]);

    for (size_t idx = 1, end = d_vsdMatrix.size(); idx != end; ++idx)
        VSD::vary(out, 4, s_noColon[idx], 'a', d_vsdMatrix[idx]);
}
