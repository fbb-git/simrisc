//#define XERR
#include "survival.ih"

RowCol Survival::cptTNM(double diameter)
{
    auto iter = find_if(d_bc.begin(), d_bc.end(),
        [&](uint16_t max)
        {
            return diameter <= max;
        }
    );

    return RowCol{ iter - d_bc.begin(), 0 };
}
