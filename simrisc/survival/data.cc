//#define XERR
#include "survival.ih"

char const *Survival::s_lungColumn[] =
{
    "lung0:",
    "lung1:",
    "lung2:",
    "lung3:",
};

char const *Survival::s_noColon[] =
{
    "lung0",
    "lung1",
    "lung2",
    "lung3"
};
