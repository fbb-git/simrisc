#ifndef INCLUDED_SURVIVAL_
#define INCLUDED_SURVIVAL_

// Tumor:
//     Survival:
//         #              value        spread      dist:
//         type:  a        .00004475   .000004392  Normal
//                b       1.85867      .0420       Normal
//                c      - .271        .0101       Normal
//                d       2.0167       .0366       Normal
//                e       1.00         .01         Normal
//
//      # 4 columns per a..d parameter, according to table S4:
//      # lungX: X is the table S4 column index
//         lung0: a        .00143      .00095      Normal
//                b       1.84559      .33748      Normal
//                c       -.22794      .07823      Normal
//                d       1.06799      .16226      Normal
//                e       1.00         .01         Normal
//
//         lung1: a        .01530      .00381      Normal
//                b       1.69434      .10979      Normal
//                c       -.19358      .02105      Normal
//                d        .66690      .03869      Normal
//                e       1.00         .01         Normal
//
//         lung2: a        .78600      .29815      Normal
//                b        .69791      .05425      Normal
//                c        .0          .0          Normal
//                d        .0          .0          Normal
//                e       1.00         .01         Normal
//
//         lung3: a       1.25148      .32305      Normal
//                b        .77852      .34149      Normal
//                c        .0          .0          Normal
//                d        .0          .0          Normal
//                e       1.00         .01         Normal
//
// S3:
//    # T-row  range      N0,M0   N1-3,M0   N1-3,M1a-b   N0-3M1c
//    prob:     0 - 11:    .756    .157      .048        .039       # T1a,b
//    prob:    11 - 21:    .703    .197      .055        .045       # T1b
//    prob:    21 - 31:    .559    .267      .095        .078       # T1c
//    prob:    31 - 51:    .345    .351      .167        .137       # T2a,b
//    prob:    51 - 71:    .196    .408      .218        .178       # T3
//    prob:    71 - * :    .187    .347      .256        .210       # T4
//
//  #  BC TNM categories up to (<=) diameters (mm):
//  BC:        20  50   *
//  #  TNM:     1   2   3


#include "../typedefs/typedefs.h"
#include "../vsd/vsd.h"
#include "../probgroup/probgroup.h"

class Survival
{
        // d_vsdMatrix is a [][4] matrix.
        // with BC simulations only row 0 is filled and used, and d_vsdRow is
        // set to 0, its default value. The member pointer 'd_cptVSDrow'
        // performs no action (cals cptTNM()).
        //
        // with LC simulations 4 rows are filled with, resp. the
        // lung0 thru lung3 VSD values, and the member pointer d_cptVSDrow
        // computes d_vsdRow using S3, S4 and the tumor diameter (calls
        // cptVSDrow()).

                                    // 1st idx: BC: always 0
                                    //          LC: 0..3 for lung0..lung4
                                    // 2nd idx: elements contain the BC/LC 
    VSDmatrix d_vsdMatrix;          //      a..d VSD params (0: a, thru 3: d)

    ProbGroup::Vector d_s3;         // contains the lines of S3
    Uint16Vect d_bc;                // contains the BC: specifications

    size_t d_vsdRow = 0;

    void (Survival::*d_vary)(std::ostream &out);
    void (Survival::*d_write)(std::ostream &out) const;

                                        // LC: cptVSDrow, BC: cptTNM
    RowCol (Survival::*d_cptVSDrow)(double diameter);

    static char const *s_lungColumn[4];           // cf. data.cc
    static char const *s_noColon[4];              // cf. data.cc

    public:
        Survival();

        bool metastatis() const;                                    // .f
        VSD const &operator[](size_t idx) const;                    // .f
                                        // determine random vsd-value(s)

        RowCol setVSD(double diameter); // with LC: cpt the row of  // .f
                                        // d_vsdMatrix to use (cptVSDRow)
                                        // with BC: cptTNM.

        void vary(std::ostream &out);   // and write to 'out'       // .f
        void writeParameters(std::ostream &out) const;              // .f

// OBS:       void vsdRow() const;  (was called in tumor/functionf.cc)

    private:
        RowCol cptVSDrow(double diameter);
        RowCol cptTNM(double diameter);

        void loadBC();
        void loadS3();
        void loadS4();

        void lungParameters(std::ostream &out) const;
        void s3parameters(std::ostream &out) const;

        void varyBreast(std::ostream &out);
        void varyLung(std::ostream &out);

        void writeBreast(std::ostream &out) const;
        void writeLung(std::ostream &out) const;
};

#include "survival.f"

//        ProbGroup::Vector const &s3() const;                        // .f

#endif
