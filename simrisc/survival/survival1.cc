#define XERR
#include "survival.ih"

Survival::Survival()
{
    if (not Globals::isBreast())                // LC simulation
    {
        d_vary = &Survival::varyLung;
        d_write = &Survival::writeLung;
        d_cptVSDrow = &Survival::cptVSDrow;

        loadS3();
        loadS4();                               // loads "lung[0-4]"
    }
    else                                        // BC simulation: only 
    {                                           // type: must be specified
        d_vary = &Survival::varyBreast;
        d_write = &Survival::writeBreast;
        d_cptVSDrow = &Survival::cptTNM;

        d_vsdMatrix.push_back(                  // load the BC 'type' params
                        Parser::VSDparameters(
                            VARY_MEAN,
                            { "type:" },
                            { 'a', 'b', 'c', 'd', 'e' }
                        )
                    );

        loadBC();
    }
}
