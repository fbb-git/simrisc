//#define XERR
#include "ct.ih"

void CT::setSensitivity()
{
    auto lines = Parser::any({ "CT:", "sensitivity:" });

    bool checkRange = true;
    while (true)
    {
        LineInfo const *line = lines.get();
        if (line == 0)
            break;

        add(&checkRange, *line);    // some failure: don't check ranges
    }

    if (d_sensitivity.size())                       // sensitivity groups
    {                                               // must cover 0..*
        if (d_sensitivity.front().group.begin() != 0 or
            not d_sensitivity.back().group.maxEnd()
        )
            Err::msgTxt(Err::CT_SENS_RANGE);
    }

}
