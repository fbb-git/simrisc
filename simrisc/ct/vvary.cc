//#define XERR
#include "ct.ih"

// override
void CT::vVary(ostream &out)
{
    d_specificity.vary();

    out << "  CT:\n"
           "    sensitivity:  f(diam), cf. config. file\n";
    //d_sensitivity.showVary(out);

    out << "    specificity:  ";
    d_specificity.showVary(out);
}
