//#define XERR
#include "err.ih"

// static
void Err::specification()
{
    msg(SPEC_ERROR) << '`' << s_lineInfo->txt << '\'' << endl;
}
