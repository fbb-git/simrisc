//#define XERR
#include "err.ih"

// static
void Err::atLeast(double minimum)
{
    msg(AT_LEAST) << '`' << s_lineInfo->txt <<
            "':  parameter must be >= " << minimum << endl;
}
