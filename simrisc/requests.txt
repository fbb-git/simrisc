﻿Wensenlijstje Simrisc

Update: 17 feb 2024

>     1. Robuust maken van de input 
> 
> Het lijkt er sterk op dat Simrisc erg gevoelig is voor het precieze format van
> de input: zijn er wel of geen meerdere spaties, volgorde van invoer, etc. Het
> zou mooi zijn als Simrisc meer robuust met de input kan omgaan.

Zie mijn mail van gisteren: hebben jullie een invoer die niet wordt
geaccepteerd?  
 
===========================================================================
Gedaan:

>     2. Ontkoppelen borstkanker en longkanker screening 
> 
> Gebruikers van borstkanker screening en longkanker screening werken wel veel
> samen, maar zijn toch ook veel met hun eigen onderzoeksgebied bezig. Als
> gebruiker van Simrisc word je bij het simuleren van borstkanker screening nu
> voortdurend ‘lastig gevallen’ met informatie over longkanker screening en
> omgekeerd. Het zou mooi zijn als dit volledig ontkoppeld wordt.

Kan dat iets preciezer worden beschreven? In de configuratiefile zijn
parameters voor beide kankers opgenomen omdat dat nou eenmaal door simrisc
wordt aangeboden, maar ws. (moet ik even testen) kunnen de parameters voor
borstkanker achterwege blijven wanneer longkankersimulaties worden uitgevoerd,
en omgekeerd. Wanneer er in de uitvoer info over het ene type wordt
geproduceerd terwijl 't andere type werd gesimuleerd kunnen jullie dan
aangeven wat niet moet worden vermeld bij de resp. simulatiemethoden?

Bij de borstkanker rounds.txt file (Marcel,Tue, 20 Feb 2024 07:44:23 +0000):

in de rounds file die ook als uitvoer wordt gegenereerd worden Mammo, Tomo en
MRI ook genoemd, ook al worden ze in rondes niet gebruikt. Voor die file ligt
't voor de hand om niet gebruikte modaliteiten niet te vermelden als ze bij de
round-specificaties niet worden genoemd.


>     3. Risico op kanker
> 
> Het risico op kanker op een bepaalde leeftijd is gedefinieerd via 3 parameters
> (f, m en s). Het zou mooi zijn als er ook de mogelijkheid zou zijn om een
> tabel met incidentie per leeftijd in te lezen.

    Dat geldt voor alle parameters bij 'Incidence:' (Marcel, Wed, 21 Feb 2024
    07:55:47 +00000)

Moeten we ook een minimum aantal waarden vereisen in zo'n tabel?

cf. Marcel Wed, 21 Feb 2024 09:43:32 +0000:

>     Leeftijd-1 indicentie-1
>     Leeftijd-2 indicentie-2
>     Etc
>     Waarbij per default
>     0 0
>     100 1
>     Bv:
>     25 0.001
>     30 0.003
>     35 0.005
>     40 0.009
>     45 0.014
> 
> Hoe dat dan met de spread moet, weet ik nog zoeven niet, even over nadenken

Geldt voor lifetiemRisk.    meanAge en stdDev: vervallen bij gebruik van een
tabel:

> de specificatie wordt dan:
>      ...
>         Male:
>             riskTable: 25 0.001 (etc)



>     4. Participatie = attendanceRate
> 
> De participatie wordt nu slechts met 1 decimaal geaccepteerd, dat zouden we
> graag in 2 decimalen willen

Wat is de participatieparameter in de config file? Noch in de config file,
noch in de simriscparams man-page wordt participatie (of participation)
genoemd 

    Aanpassen in Screening::writeParameters(ostream &out) op 't aantal
    gewenste parameters

    Marcel (Wed, 21 Feb 2024 07:48:49 +0000): 3 decimalen

 
>     5. Definitie van interval: ‘echte’ interval tumoren toevoegen
> 
> Bij het bepalen of een tumor een interval tumor is, wordt nu geen rekening
> gehouden met of de vrouw wel of niet naar de voorgaande screening is
> gegaan. Het huidige aantal willen we graag behouden, maar daarnaast zouden we
> graag het aantal intervallen hebben waarin alleen de tumoren worden meegenomen
> na een bijgewoonde screeningronde.

OK, (d_nTrueIntervals in loop/) (Wed, 21 Feb 2024 07:44:09 +0000)

>     6. Output detection round: verander nummering
> 
> Detection round geeft aan als er een gedetecteerde tumor is en in welke ronde
> deze is gedetecteerd. Deze begint voor screening ronde 1 bij 0 en telt zo door
> tot 12 (in normale NL setting). Een 0 wordt ook gegeven als de tumor niet
> wordt gedetecteerd. We zouden de nummering daarom graag aanpassen, zodat deze
> 0en makkelijker te onderscheiden zijn.

    -> wachten op Keris (Marcel Wed, 21 Feb 2024 07:43:03 +0000)

    Keris Mon, 26 Feb 2024 14:59:40 +0000:

>          -1         tumor gedetecteerd voor de eerste screeningsronde
>           0         tumor gedetecteerd na de eerste screeningsronde (en dus
>                     voor de 2e screeningsronde)
>          *0         tumor gedetecteerd na de eerste screeningsronde, die niet
>                     is bijgewoond
>           2         tumor gedetecteerd na de derde screeningsronde
>          *2         tumor gedetecteerd na de derde screeningsronde, die niet
>                     is bijgewoond
>           9         tumor gedetecteerd na de laatste screeningsronde
>          *9         tumor gedetecteerd na de laatste screeningsronde, die niet
>                     is bijgewoond

wordt (screeningsrondes genummerd van 1..N):
            0         geen tumor
           -1         tumor gedetecteerd voor de eerste screeningsronde
            1         tumor gedetecteerd na de eerste screeningsronde (en dus
                      voor de 2e screeningsronde)
           *1         tumor gedetecteerd na de eerste screeningsronde, die niet
                      is bijgewoond
            3         tumor gedetecteerd na de derde screeningsronde
           *3         tumor gedetecteerd na de derde screeningsronde, die niet
                      is bijgewoond
            N         tumor gedetecteerd na de laatste screeningsronde
           *N         tumor gedetecteerd na de laatste screeningsronde, die niet
                      is bijgewoond
screeningsrondes in rounds.txt worden ook genummerd van 1..N


>     7. Input in zelfde eenheid, niet logaritmisch
> 
> Verschillende input van simrisc wordt nu logaritmisch gegeven, bijvoorbeeld
> gemiddelde grootte zelf-detectie, waar andere input in mm wordt gegeven, bijv
> startgrootte tumoren. We zouden graag alle input in mm geven en de logaritme
> door het model laten berekenen.

Over welke parameters gaat 't? Zijn dat (alle) 'mean' parameters bij Growth
(waarbij in de config file 'selfDetect' wordt genoemd) of zijn 't (ook) andere
parameters? Bij die gemiddelden worden ook stddev en spread genoemd. Moeten
die zo blijven of moeten die ook worden aangepast (bv selfDetect breast:
spread is nu .084, e^.084 = 1.08763)?

Op dit moment in Growth:

# values must be changed to exp(value)
        #selfDetect:      # stdev       mean    spread  dist
        breast:             .70         2.92    .084    Normal
        lung:               .014        3.037   .61     Normal

        DoublingTime:
            #                   stdev   mean  spread  dist.
# values must be changed to exp(value)
            ageGroup:  1 - 50:   .61    4.38   .43    Normal
            ageGroup: 50 - 70:   .26    5.06   .17    Normal
            ageGroup: 70 - * :   .45    5.24   .23    Normal

            #     all ages      stdev   mean  spread  dist.
# values must be changed to exp(value)
            lung:                .21    4.59   .74    Normal

 





