#define XERR
#include "globals.ih"

// static
void Globals::setSimType(std::string const &value)
{
    s_simType =
        static_cast<SimulationType>(
            find(s_label[OPTION], s_label[OPTION + 1], value) -
                                                            s_label[OPTION]
        );

//    xerr("set simtype to " << value << ": " << s_simType);
}
