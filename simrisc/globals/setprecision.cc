//#define XERR
#include "globals.ih"

// static
ostream &Globals::setPrecision(ostream &out, uint16_t nDigits)
{
    out.setf(ios::fixed, ios::floatfield);
    out.precision(nDigits);
    return out;
}
