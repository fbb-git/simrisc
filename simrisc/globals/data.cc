//#define XERR
#include "globals.ih"

uint16_t const Globals::uint16_max = numeric_limits<uint16_t>::max();

bool Globals::s_vary = false;
SimulationType Globals::s_simType = BREAST;
double const Globals::s_sqrt2PI = sqrt(2 * M_PI);
unsigned Globals::s_width = 0;

//char const *Globals::s_label[][3] =     // update when enums
//{                                       // SimulationType is modified
//     { "Breast:", "breast:", "breast" },// the top line represents the default
//     { "Male:",   "male:",   "male"   },
//     { "Female:", "female:", "female" }
//};

char const *Globals::s_label[][3] =         // update when enums
{                                           // SimulationType is modified
     { "Breast:", "Male:",  "Female:" },    // the first elements represent
     { "breast:", "male:",  "female:" },    // the default
     { "breast",  "male",   "female"  }
};
