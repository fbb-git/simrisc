#ifndef INCLUDED_BETADIST_
#define INCLUDED_BETADIST_

#include <iosfwd>
#include <random>

    // 'Beta' has just been read     | read from here
    // # LC:    eta   beta     dist    constant factor   aParam    bParam
    // male:    -1.4   .32     Beta    .234091  1.72727  2.664237  5.184883
    // female:  -1.4  1.40     Beta    .744828   .818966 3.366115  4.813548

class BetaDist
{
    double d_param[4];      // 0: constant, 1: factor, 2: aParam, 3: bParam

    std::gamma_distribution<> d_xGamma;
    std::gamma_distribution<> d_yGamma;

    public:
        BetaDist(std::istream &in); // extracts params from the config file
        void reset();
        double operator()(std::mt19937 &engine);
        std::pair<size_t, double const *> parameters() const;
};

inline std::pair<size_t, double const *> BetaDist::parameters() const
{
    return {std::size(d_param), d_param};
}

#endif
