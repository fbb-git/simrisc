//#define XERR
#include "betadist.ih"

double BetaDist::operator()(mt19937 &engine)
{
    double xValue = d_xGamma(engine);
    return
        (xValue / (xValue + d_yGamma(engine)) + d_param[0]) / d_param[1];
}
