#define XERR
#include "tumor.ih"

//    if (0.120662 < rndTumor and rndTumor < 0.120664)
//    {
//        cerr.setf(ios::fixed, ios::floatfield);
//
//        for (double value: d_tumorInfo.cumTotalRisk())
//            cerr << setw(6) << value << '\n';
//    }


bool Tumor::tumorAge()
{
    Random &random = Random::instance();

    double rnd;

    d_selfDetectAge = findAge(
                            d_tumorInfo.cumTotalRisk(),
                            rnd = random.uniformCase());

    g_log << "    tumorAge: self detect age: " << d_selfDetectAge << '\n';

    Options::instance().fixedTumorAge(d_selfDetectAge);

    if (d_selfDetectAge <= MAX_AGE)
        return true;

    d_selfDetectAge = NO_TUMOR;

    random.uniformCase();             // extra calls to synchronize the
    random.logNormal(0, 0);           // random generator between
    random.logNormal(0, 0);           // tumor and no tumor states

    g_log << "    no tumor, generated 3 synchronization random numbers\n";

    return false;
}

//xerr("random tumor prob: " << rnd << ", age " << d_selfDetectAge);


//    g_log << "    cum total risk:\n";
//    for (size_t idx = 0, end = d_tumorInfo.cumTotalRisk().size();
//            idx != end;
//                ++idx)
//        g_log << d_tumorInfo.cumTotalRisk()[idx] <<
//            ((idx + 1) % 10 == 0 ? "\n" : ", ");
//    g_log << '\n';

