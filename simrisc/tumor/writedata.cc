//#define XERR
#include "tumor.ih"

void Tumor::writeData(CSVTable &tab) const
{
    tab.more() << s_noYes[d_present] <<                             // 6
                  s_noYes[d_found] <<                               // 7
                  s_noYes[d_interval];                              // 8

            // see modifications (8)
    if (d_diameter > 1000)                                          // 9
        tab.more() << "1001";                   // was: 'inf'
    else
        tab.more() << d_diameter;

            // see also modifications (7)
            // //setw(6) << d_selfDetectAge == NO_TUMOR ? 0 : age << '\t' <<

    tab.more() << checkNot(d_doublingDays) <<                       // 10
                  checkNot(d_prePeriod) << checkNot(d_onset) <<     // 11 - 12
                  checkNot(d_selfDetectAge) << d_deathAge;          //  13 - 14
}
