//#define XERR
#include "tumor.ih"

double Tumor::volume() const
{
    requireTumor("volume");
    return d_volume;
}
