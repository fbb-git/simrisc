//#define XERR
#include "tumor.ih"

void Tumor::requireTumor(char const *type) const
{
    if (d_selfDetectAge == NO_TUMOR)
        throw Exception{} << "cannot determine tumor " << type <<
                             " for a non-existing tumor";
}
