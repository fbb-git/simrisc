#define XERR
#include "tumor.ih"

void Tumor::reset()
{
    g_log << "  Tumor::reset\n";

    d_found = false;
    d_interval = false;

    d_present = false;                  // see 'characteristics()'
    d_volume = 0;
    d_diameter = 0;
    d_deathAge = 0;

    if (not tumorAge())
    {
        d_pSurvival = 1;
        d_startVolume = 0;              // see modifications (7)
        d_doublingDays = NO_TUMOR;
        d_doublingYears = NO_TUMOR;
//        d_selfDetect = 0;
        d_prePeriod = NO_TUMOR;
        d_onset = NO_TUMOR;
        return;
    }

    uint16_t idx = ageGroup();

    Random &random = Random::instance();

    d_pSurvival = random.uniformCase();

    d_startVolume = volume(d_growth.start());

    AgeGroupVSD const &group = d_growth.ageGroupVSD(idx);

    d_doublingDays = random.logNormal(group.value(), group.stdDev());

    d_doublingYears = d_doublingDays / N_YEARDAYS;

    double selfDetectDiam;

    while (true)
    {
        selfDetectDiam = random.logNormal(d_growth.selfMu().value(),
                                             d_growth.selfSigma());
        g_log << "try selfDetectDiam = " << selfDetectDiam << '\n';
        if (selfDetectDiam > d_growth.start())
            break;
    }

    d_prePeriod =   d_doublingDays *
                    log(
                        volume(selfDetectDiam) /
                        volume(d_growth.start())
                    ) * s_log2YearInv;

    d_onset = d_selfDetectAge - d_prePeriod;

    g_log << "\nTUMOR RESET:\n"
             "  selfDetectAge =  " << d_selfDetectAge << "\n"
             "  preperiod =      " << d_prePeriod << "\n"
             "  onset =          " << d_onset << "\n"
             "  doubingDays =    " << d_doublingDays << "\n"
             "  selfDetectDiam = " << selfDetectDiam << "\n"
             "  volume(..) =     " << volume(selfDetectDiam) << "\n"
             "  growth start =   " << d_growth.start() << "\n"
             "  volume(..) =     " << volume(d_growth.start()) << "\n"
             "  p(survival) =    " << d_pSurvival << '\n';
}




