//#define XERR
#include "tumor.ih"

void Tumor::checkDeath(double naturalDeathAge)
{
    if (naturalDeathAge < d_onset)              // death before tumor
    {
        d_present = false;
        d_found = false;
        d_interval = false;
    }
}
