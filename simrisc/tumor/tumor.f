inline double Tumor::selfDetectAge() const
{
    return d_selfDetectAge;
}

inline double Tumor::onset() const
{
    return d_onset;
}

inline bool Tumor::at(double age) const
{
    return d_onset <= age;
}

inline bool Tumor::selfDetectBefore(double age) const
{
    return d_selfDetectAge <= age;
}

// inline bool Tumor::selfDetectable() const
// {
//     return d_selfDetectAge != NO_TUMOR;
// }
// 
// inline void Tumor::characteristics()
// {
//     characteristics(d_selfDetectAge);
// }

inline void Tumor::intervalCancer()
{
    d_interval = true;
}

inline void Tumor::found()
{
    d_found = true;
}

inline void Tumor::setDeathAge()
{
    setDeathAge(d_selfDetectAge);
}

inline double Tumor::deathAge() const
{
    return d_deathAge;
}

inline double Tumor::diameter() const
{
    return d_diameter;
}

inline bool Tumor::metastasis() const
{
    return d_survival.metastatis();
}

inline bool Tumor::present() const
{
    return d_present;
}

inline std::ostream &operator<<(std::ostream &out, Tumor const &tumor)
{
    return tumor.insert(out);
}

inline RowCol const &Tumor::rowCol() const
{
    return d_rowCol;
}

