//#define XERR
#include "probgroup.ih"

ProbGroup::ProbGroup(Series series)
:
    d_group(series),
    d_prob(N_PROBCATS),
    d_cumProbs(N_PROBCATS),
    d_breastCancer(Globals::isBreast())
{}
