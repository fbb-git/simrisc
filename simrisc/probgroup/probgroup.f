inline uint16_t ProbGroup::begin() const
{
    return d_group.begin();
}

inline bool ProbGroup::contains(double value) const
{
    return d_group.contains(value);
}

inline uint16_t ProbGroup::end() const
{
    return d_group.end();
}

inline Group const &ProbGroup::group() const
{
    return d_group;
}

inline std::vector<double> const &ProbGroup::prob() const
{
    return d_prob;
}

inline double ProbGroup::prob(size_t idx) const
{
    return d_prob[idx];
}

inline bool ProbGroup::sumOne() const
{
    return Globals::isZero(d_cumProbs[N_PROBCATS - 1] - 1);
}

inline std::istream &operator>>(std::istream &in, ProbGroup &probgroup)
{
    return probgroup.extract(in);
}

inline std::ostream &operator<<(std::ostream &out, ProbGroup const &probgroup)
{
    return probgroup.insert(out);
}
