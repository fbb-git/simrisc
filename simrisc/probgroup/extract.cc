//#define XERR

#include "probgroup.ih"

//  E.g.,
//     ageGroup:    0  - 40: ->
//                              .05    .30    .48    .17
//  or (e.g., S3):
//      prob:      20:  ->
//                              .703    .197      .055        .045

istream &ProbGroup::extract(istream &in)
{
    in >> d_group;                              // extract the group

    double cumProb = 0;

    for (size_t idx = 0; idx != N_PROBCATS; ++idx)
    {
        double probability;
        in >> probability;

        d_prob[idx] = probability;

        cumProb += probability;
        d_cumProbs[idx] = cumProb;
    }

    return in;
}
