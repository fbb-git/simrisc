//#define XERR
#include "probgroup.ih"

char const *ProbGroup::s_labels[][2] =
{
    { "diameter: ", ", prob.: "  },         // labels for lung cancer sim.
    { "ageGroup: ", ", bi-rad: " }          // labels for breast cancer sim.
};
