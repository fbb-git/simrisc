#define XERR
#include "log.ih"

void Log::set(unsigned begin, unsigned end, std::string const &fname,
              string const &ignored)
{
    if (d_set)                          // activating log can only be done
        return;                         // once

    if (begin > end)
        throw Exception{} << "--log: " << begin << " thru " << end <<
                    " 1st value must be <= 2nd value";

    d_logFile = Exception::factory<ofstream>(fname);

    *this << "Logging cases " << begin << " thru " << end << '\n';

    d_set = true;
    d_begin = begin;
    d_end = end;
    d_ignored = ignored;
}
