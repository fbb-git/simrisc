#include "../log.h"

using namespace std;

int main()
{
    g_log << "nothing\n";

    g_log.set(4, 5, "out");

    g_log.caseIdx(1);

    g_log << "nothing\n";

    g_log.caseIdx(4);

    g_log << "special line for case 4\n";

    g_log.caseIdx(6);

    g_log << "nothing\n";

}
