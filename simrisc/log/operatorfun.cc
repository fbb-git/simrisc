#define XERR
#include "log.ih"

size_t count = 0;

Log &Log::operator()(int ignore)
{
    if (d_active and d_ignored.find(ignore) != string::npos)
    {
        d_ignore = true;
        d_active = false;
    }

    return *this;
}
