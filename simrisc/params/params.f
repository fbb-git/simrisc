inline void Params::cptTumorRisk(DoubleVect &ageValues)
{
    v_cptTumorRisk(ageValues);
}

inline double Params::prob() const
{
    return d_prob;
}

inline void Params::invalid()
{
    d_prob = -1;
}

inline void Params:: vary(std::ostream &out)
{
    v_vary(out);
}

inline std::string const &Params::label() const
{
    return s_label[d_idx];
}

// static
inline std::string const &Params::label(size_t idx) 
{
    return s_label[idx];
}

// static
inline size_t Params::nLabels()
{
    return s_label.size();
}



