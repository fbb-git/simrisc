//#define XERR
#include "growth.ih"

void Growth::vary(ostream &out)
{
    d_selfMean.vary();

    out << "  Growth:\n"
           "    selfDetect:  ";

    d_selfMean.showVary(out);
    out.put('\n');

    AgeGroupVSD::vary(out, 2,   // d_doublingTime.size(),
                           "DoublingTime:", d_doublingTime);
}
