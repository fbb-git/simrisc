//#define XERR
#include "growth.ih"

AgeGroupVSD const &Growth::ageGroupVSDbreast(uint16_t idx) const
{
    return d_doublingTime[idx];
}
