#define XERR
#include "growth.ih"

Growth::Growth()
:
    d_base{ "Growth:", "" },
    d_bc{ Globals::isBreast() },
    d_selfMean(VARY_NONNEG)
{
    setSelfDetect();
    setDoublingTime();

    setStart();
}
