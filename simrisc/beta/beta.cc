#include <iostream>
#include <ctime>
#include <random>

using namespace std;

mt19937 engine(time(0));

int main()
{
//    gamma_distribution<> x(25.50470);       // male obsolete
//    gamma_distribution<> y(53.88666);

    gamma_distribution<> x(2.664237);       // male
    gamma_distribution<> y(5.184883);

    double aValue = .234091;
    double fValue = 1.72727;

//    gamma_distribution<> x(5.050283);       // female obsolete
//    gamma_distribution<> y(7.506246);

//    gamma_distribution<> x(3.366115);       // female
//    gamma_distribution<> y(4.813548);
//
//    double aValue = 0.744828;
//    double fValue = 0.818966;

//    cout << "betafem = c(";

    for (size_t idx = 0; idx < 1000; ++idx)
    {
        double xValue = x(engine);

        double generated = xValue / (xValue + y(engine));

        cout <<
                (
                    (generated + aValue) / fValue
                ) << ',';

//        if (++idx % 8 == 0)
//            cout << '\n';
    }
    cout << "\n";
}
