inline Distribution const &VSD::distribution() const
{
    return d_dist;
}

inline void VSD::vary()
{
    d_value = d_dist.value(d_orgValue);
}

inline double VSD::value() const
{
    return d_value;
}

inline DistType VSD::distType() const
{
    return d_dist.type();
}

inline std::string const &VSD::distName() const
{
    return Distribution::name(d_dist.type());
}

inline std::ostream &operator<<(std::ostream &out, VSD const &vsd)
{
    return vsd.insert(out);
}

inline std::ostream &operator<<(std::ostream &out, VSDvect const &vect)
{
    return VSD::insert(out, vect);
}

inline std::istream &operator>>(std::istream &in, VSD &vsd)
{
    return vsd.extract(in);
}

//inline double VSD::spread() const
//{
//    return d_dist.value();
//}
