#define XERR
#include "vsd.ih"

std::ostream &VSD::insert(std::ostream &out) const
{
    return Globals::setWidthPrec(out, 6, 3) <<
        (d_exp ? exp(d_orgValue) : d_orgValue) << ",  " << d_dist;
}
