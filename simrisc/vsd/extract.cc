#define XERR
#include "vsd.ih"

// value   spread  dist.
// 21.1    0.048   Normal
// 16.51                    // implies spread = false
//
//                beta -> d_value
//
// # LC:    eta   beta     dist    constant factor   aParam    bParam
// male:    -1.4   .32     Beta    .234091  1.72727  2.664237  5.184883
// female:  -1.4  1.40     Beta    .744828   .818966 3.366115  4.813548

istream &VSD::extract(istream &in)
{
    string v1;
    string v2;

    in >> d_value;                      // the beta parameter of distributions
    d_orgValue = d_value;

    if (Globals::vary())
    {
        in >> d_dist;
        (*d_valueCheck)(d_value);       // check valid SD / Prob. values
    }

    return in;
}
