//#define XERR
#include "vsd.ih"

// static
void VSD::probCheck(double &value)
{
    if (not Globals::proportion(value))
    {
        Err::range();
        value = 0;
    }
}
