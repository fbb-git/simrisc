//#define XERR
#include "vsd.ih"

// static
void (*VSD::s_valueCheck[])(double &) =
{
    &accept,        // VARY_MEAN
    &posCheck,
    &probCheck,

    &accept,         // VARY_BETA_...
    &accept,
};

// static
VaryType VSD::s_varyType[] =
{
    VARY_MEAN,
    VARY_NONNEG,
    VARY_PROB,
    VARY_BETA_MALE,
    VARY_BETA_FEMALE,
};

unsigned    VSD::s_indent = 0;
char const *VSD::s_type = 0;
char        VSD::s_ch = 0;
unsigned    VSD::s_width = 0;
unsigned    VSD::s_prec = -1;
