#define XERR
#include "vsd.ih"

VSD::VSD(VaryType varyType)
:
    d_dist(varyType),
    d_valueCheck(s_valueCheck[varyType]),
    d_exp(false)
{}
