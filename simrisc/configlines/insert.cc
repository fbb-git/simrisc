//#define XERR
#include "configlines.ih"

ostream &ConfigLines::insert(ostream &out) const
{
    for (auto iter = d_iter, end = d_config.end(); iter != end; ++iter)
        out << *iter << '\n';

    return out;
}
