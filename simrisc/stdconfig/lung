# supported distribution names are: Normal, LogNormal, Uniform, Beta
# Modalities are: Mammo, Tomo, MRI, CT

Scenario:
                                # use true for variable spreading
#    spread:                 true

    spread:                 false

    iterations:             1

                                # random generator behavior:
                                # random, fixed, increasing
    generator:              fixed

                                # initial seed unless using generator: random
    seed:                   1

                                # n cases to simulate
    cases:                  1000

    # Used for all modalities except for CT:
    #   ref.age proportion  biop  specs (first diameter must be 0)
    # Lungcancer costs: configured at Modalities: CT: costs:

Costs:
    biop:       176
    diameters:  0: 6438  20: 7128  50: 7701
    Discount:       # breast    lung
        proportion:      0      .04
        age:            50

BreastDensities:
    #                bi-rad:  a      b      c      d
    ageGroup:    0  - 40:    .05    .30    .48    .17
    ageGroup:    40 - 50:    .06    .34    .47    .13
    ageGroup:    50 - 60:    .08    .50    .37    .05
    ageGroup:    60 - 70:    .15    .53    .29    .03
    ageGroup:    70 - * :    .18    .54    .26    .02

Modalities:

    CT:
        #        screening    diagnosis    M0      M1   (M0, M1: Table S3)
        costs:      176         1908      37909   56556

        dose:  1

        #               diam.   value (must be integral 0..100 or -1)
        sensitivity:    0 - 3:  0
        sensitivity:    3 - 5:  -1  # formula: (.5 * diam - 1.5) * 100
        sensitivity:    5 - *:  100

        #               mean    stddev  dist
        specificity:    99.2    .076    Normal

    Mammo:
        costs:          64

        #      bi-rad:  a       b       c       d
        dose:           3       3       3       3
        m:             .136    .136    .136    .136

        #             ageGroup
        specificity:  0 - 40:  .961     40 - *: .965

        #       1       2       3        4
        beta:  -4.38    .49     -1.34    -7.18

        systematicError:  0.1

    Tomo:
        costs:         64

        #      bi-rad:  a       b       c       d
        dose:           3       3       3       3
        sensitivity:    .87     .84     .73     .65

        #             ageGroup
        specificity:  0 - 40:  .961     40 - *: .965

    MRI:
        costs:          280
        sensitivity:    .94
        specificity:    .95

Screening:
    # round: age    space separated modalities.
    # subsequent ages must exceed the last age by at least 1
    # with lung cancer simulations only CT can be specified,
    # with breast cancer simulations CT cannot be specified,
    round:     50  CT
    round:     52  CT
    round:     54  CT
    round:     56  CT
    round:     58  CT
    round:     60  CT
    round:     62  CT
    round:     64  CT
    round:     66  CT
    round:     68  CT
    round:     70  CT
    round:     72  CT
    round:     74  CT

    #                   proportion:
    attendanceRate:     .8

Tumor:

    Beir7:
            #    eta   beta  spread   dist.
        breast:  -2.0   0.51   0.32  Normal

        #                            Beta-distribution parameters:
        # LC:    eta   beta  dist    constant factor   aParam    bParam
        male:    -1.4   .32  Beta    .234091  1.72727  2.664237  5.184883
        female:  -1.4  1.40  Beta    .744828   .818966 3.366115  4.813548

    Growth:
            #  breast   lung
        start:    5       3

        #selfDetect:      # stdev       mean    spread  dist
        breast:             .70         2.92    .084    Normal
        lung:               .014        3.037   .061    Normal

        DoublingTime:
            #                   stdev   mean  spread  dist.
            ageGroup:  1 - 50:   .61    4.38   .43    Normal
            ageGroup: 50 - 70:   .26    5.06   .17    Normal
            ageGroup: 70 - * :   .45    5.24   .23    Normal

            #     all ages      stdev   mean  spread  dist.
            lung:                .21    4.59   .74    Normal

    Incidence:
        Male:
            #                   value   spread  distr.
            lifetimeRisk:         .22   .005    Normal
            meanAge:            72.48  1.08     Normal
            stdDev:              9.28  1.62     Normal

        Female:
            #                   value   spread  distr.
            lifetimeRisk:         .20   .004    Normal
            meanAge:            69.62  1.49     Normal
            stdDev:              9.73  1.83     Normal

        Breast:
            probability:    1
            #                   value   spread  distr.
            lifetimeRisk:         .226  .0053   Normal
            meanAge:            72.9    .552    Normal
            stdDev:             21.1

        BRCA1:
            probability:    0
            #                   value   spread  distr.
            lifetimeRisk:         .96
            meanAge:            53.9
            stdDev:             16.51

         BRCA2:
            probability:    0
            #                   value   spread  distr.
            lifetimeRisk:         .96
            meanAge:            53.9
            stdDev:             16.51

    Survival:
        #              value        spread      dist:
        type:  a        .00004475   .000004392  Normal
        type:  b       1.85867      .0420       Normal
        type:  c       -.271        .0101       Normal
        type:  d       2.0167       .0366       Normal

       # table S4: 4 columns per a..d parameter
       # lungX: X is table S4's column index
       lung0: a        .00143      .00095      Normal
       lung0: b       1.84559      .33748      Normal
       lung0: c       -.22794      .07823      Normal
       lung0: d       1.06799      .16226      Normal

       lung1: a        .01530      .00381      Normal
       lung1: b       1.69434      .10979      Normal
       lung1: c       -.19358      .02105      Normal
       lung1: d        .66690      .03869      Normal

       lung2: a        .78600      .29815      Normal
       lung2: b        .69791      .05425      Normal
       lung2: c        .0          .0          Normal
       lung2: d        .0          .0          Normal

       lung3: a       1.25148      .32305      Normal
       lung3: b        .77852      .34149      Normal
       lung3: c        .0          .0          Normal
       lung3: d        .0          .0          Normal

    S3:
       # T-row  range      N0,M0   N1-3,M0   N1-3,M1a-b   N0-3M1c
       prob:     0 - 11:    .756    .157      .048        .039       # T1a,b
       prob:    11 - 21:    .703    .197      .055        .045       # T1b
       prob:    21 - 31:    .559    .267      .095        .078       # T1c
       prob:    31 - 51:    .345    .351      .167        .137       # T2a,b
       prob:    51 - 71:    .196    .408      .218        .178       # T3
       prob:    71 - * :    .187    .347      .256        .210       # T4
