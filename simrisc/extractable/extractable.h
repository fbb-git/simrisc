#ifndef INCLUDED_EXTRACTABLE_
#define INCLUDED_EXTRACTABLE_

    // concept used by Parser's extract templates to ensure that
    // the arguments to extract can actually be extracted

template <typename Type>
concept Extractable =
    requires(std::istream in, Type value)
    {
        in >> value;
    };

#endif
