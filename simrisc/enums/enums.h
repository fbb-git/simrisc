#ifndef INCLUDED_ENUMS_
#define INCLUDED_ENUMS_

#include <cstdint>

static unsigned const END_AGE = 101;
static unsigned const MAX_AGE = END_AGE - 1;

enum: uint16_t
{
    UNDEFINED = static_cast<uint16_t>(~0U)
};


enum FirstChar      // 1st char is capital or LC, used by Globals
{                   // OPTION is the allowed value with --cancer (-c)
    CAPS,
    LC,
    OPTION
};

enum VaryType
{
    VARY_MEAN,                  // every value is OK
    VARY_NONNEG,                // varied values must be >= 0
    VARY_PROB,                  // varied values must be probabilities
    VARY_BETA_MALE,             // used for the Beta distributions
    VARY_BETA_FEMALE,
};

enum DistType                   // Distribution::s_name when modified
{
    UNIFORM,                    // always used distributions
    UNIFORM_CASE,
    UNIFORM_VSD,
    LOGNORMAL,

    N_STD_DISTRIBUTIONS,

    NORMAL_VARY = N_STD_DISTRIBUTIONS,

    UNIFORM_VARY,
    UNIFORM_VSD_VARY,
    LOGNORMAL_VARY,

    BETA_VARY,                  // only used when parameter spead

    N_DISTRIBUTIONS
};

enum GeneratorType
{
    FIXED_SEED,
    INCREASING_SEED,
    RANDOM_SEED,
};

enum ParamsSrc
{
    CONFIGFILE,
    ANALYSIS,
    NONE,
};

enum Series         // see (Prob)Group
{
    RANGE,          // e.g., 0 - 40:
    SINGLE,         // e.g.,   20
};

enum SimulationType // update globals/data.cc when modified
{
    BREAST,
    MALE,           // = LC simulation
    FEMALE,         // = LC simulation
};

#endif
