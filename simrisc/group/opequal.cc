//#define XERR
#include "group.ih"

bool Group::operator==(Group const &other) const
{
    return d_begin == other.d_begin and d_end == other.d_end;
}
