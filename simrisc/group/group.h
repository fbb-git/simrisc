#ifndef INCLUDED_GROUP_
#define INCLUDED_GROUP_

#include <iosfwd>
#include <ostream>
#include <cstdint>

#include "../enums/enums.h"
#include "../typedefs/typedefs.h"
#include "../hasgroup/hasgroup.h"

    // group specifications MUST end in ':'

class Group
{
    friend std::istream &operator>>(std::istream &in, Group &group);    //  .f
    friend std::ostream &operator<<(std::ostream &out, Group const &group);

    Series      d_series;

    uint16_t    d_begin = 0;
    uint16_t    d_end = END_AGE;

    public:
        Group(Series series);                                           // .f

        uint16_t begin() const;                                         // .f
        uint16_t end() const;                                           // .f

            // Err::NOT_CONSECUTIVE if false
        template <HasGroup Type>                                      // .f
        bool nextRange(std::vector<Type> const &vect) const;

        bool operator==(Group const &other) const;               // != in  .f
        bool contains(double value) const;

        bool maxEnd();          // true: changed END_AGE to max uint16_t value

    private:
        std::istream &extract(std::istream &in);
                                    // error message if not connecting
        bool connects(Group const &previous) const;
};

#include "group.f"

#endif
