//#define XERR
#include "group.ih"

bool Group::maxEnd()
{
    if (d_end != END_AGE)
        return false;

    d_end = Globals::uint16_max;
    return true;
}
