The original program uses the following condition to determine whether
prescreening must be performed:
    verb(
    if (Nscr > 0 && (naturalDeathAge < 1st screening age || (tumor present
        && tumor.selfDetectAge() < 1st screening age)))
    )
    This results in a needlessly complex implementation of the pre-screening
phase. It's much simpler to use the complement of this expression, skipping the
pre-screening phase if the complementary condition is true. The pre-screening
phase is therefore skipped if the following condition holds true:
    verb(
    not (Nscr > 0 && (naturalDeathAge < 1st screening age || (tumor present
        && tumor.selfDetectAge() < 1st screening age)))
    )
    The expression can be simplified using De Morgan's rule
tt(a && b == !a || !b):
    verb(
    not (Nscr > 0) or
    not (
         naturalDeathAge < 1st screening age or
         (tumor present and tumor.selfDetectAge() < 1st screening age)
        )
    )
    Consequently, pre-screening is skipped if there are no screening rounds
(not (Nscr > 0)) and also if the following condition holds true:
    verb(
    not (
        naturalDeathAge < 1st screening age or
        (tumor present and tumor.selfDetectAge() < 1st screening age)
    )
    )

Distributing the not-operator over the terms of the above condition, and
applying De Morgan's rule tt(!(a || b) == !a && !b) we get:
    verb(
        naturalDeathAge >= 1st screening age and
        not (
            tumor present and
            tumor.selfDetectAge() < 1st screening age
        )
    )

Applying De Morgan's rule once more this finally results in:
    verb(
        naturalDeathAge >= 1st screening age and
        (
            not tumor present or
            tumor.selfDetectAge() >= 1st screening age
        )
    )

Thus, pre-screening is skipped if the above condition holds true.
