//#define XERR
#include "densities.ih"

// static
bool Densities::percentages(vector<double> const &vect)
{
    return
        find_if(vect.begin(), vect.end(),
            [&](double value)
            {
                return value < 0 or 1 < value;  // if not in [0..1] then
            }                                   // not a percentage ->
        )                                       // vect.end() is not returned
        == vect.end();
}
