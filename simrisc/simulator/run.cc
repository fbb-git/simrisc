#define XERR
#include "simulator.ih"

//code
void Simulator::run()
{
    while (d_next)
    {
        uint16_t lineNr = d_lineNr;
                                            // read the next analysis specs
        string spec = (this->*d_nextSpecs)();   // cmdLineAnalysis() or
                                                // fileAnalysis()

        emsg.setCount(0);

        Analysis analysis{ istringstream{ spec }, lineNr };

        analysis.run();                     // run the simulation
    }
}
//=
