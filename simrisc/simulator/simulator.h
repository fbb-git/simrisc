#ifndef INCLUDED_SIMULATOR_
#define INCLUDED_SIMULATOR_

#include <fstream>
#include <cstdint>

// analyses specfy different parameters, like screeningRound, iterations,
//  nWomen

class Simulator
{
//data
    bool d_next = false;                        // true if 'run()' should run
                                                // an analysis

    uint16_t d_lineNr = 1;                      // updated by 'fileAnalysis'
    std::ifstream d_ifstream;                   // multiple analysis specs

    std::string (Simulator::*d_nextSpecs)();    // ptr to function handling
                                                // the (next) analysis spec.
//=
    public:
        Simulator();

        void run();

    private:
        bool nextAnalysis();

        void setAnalysisSource();       // extra specifications either on the
                                        // cmd line or the next 'analysis:'
                                        // specification from file

        std::string cmdLineAnalysis();
        std::string fileAnalysis();
        std::string endOfSpecs();
};

#endif
