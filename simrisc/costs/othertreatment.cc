#define XERR
#include "costs.ih"

//code
double Costs::otherTreatment(double age, double diameter,
                                         bool metastasis) const
{
    return discount(age, usingAt(age) + cost(diameter));
}
//=
