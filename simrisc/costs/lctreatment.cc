#define XERR
#include "costs.ih"

//code
double Costs::lcTreatment(double age, double diameter, bool metastasis) const
{
    return discount(age, d_treatmentCosts[metastasis]);
}
//=
