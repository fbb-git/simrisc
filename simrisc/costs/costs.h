#ifndef INCLUDED_COSTS_
#define INCLUDED_COSTS_

// Costs:
//     biop:       176
//     diameters:  0: 6438  20: 7128  50: 7701
//     Discount:
//         age:            50
//                     # breast  lung
//         proportion:      0      4

// CT:
//     #        screening    diagnosis    M0      M1   (M0, M1: Table S3)
//     costs:      176         1908      37909   56556
//                 ^^^
//                 |
//                 extracted by ModBase::costBase


#include "../typedefs/typedefs.h"
#include "../parser/parser.h"

class Costs
{
              // lower diam., cost
    typedef std::pair<double, size_t>     CostPair;

    StringVect d_base;

                                            // costs of treatments given
    std::vector<CostPair> d_treatmntPair;   // tumor diam.

    double d_referenceAge;                  // discount reference age
    double d_discountProportion;

    size_t d_biopCosts;                     // set by usingCosts
    size_t d_usingCosts;                  // for lungCancer: diagnosis,
                                            // otherwise biopsy cost

    std::vector<size_t> d_treatmentCosts; // for lungCancer: M0/M1 states

                                            // lcTreatment or otherTreatment
    double (Costs::*d_treatment)(double age, double diameter,
                                             bool metastasis) const;

    public:
        Costs();


        double usingAt(double age) const;                           // .f
    
        double screening(double age, size_t cost) const;          // .f

        double treatment(double age, double diameter,
                                     bool metastasis) const;        // .f

        void writeParameters(std::ostream &out) const;

    private:
        bool extractDiameters(Parser::Lines &&lines);
        void setAge();
        void setDiameters();
        void setDiscount(bool lungCancer);
        void setLungCancer();                       // sets LC diagnosis costs
                                                    // as d_usingCosts
        void setProportion(bool lungCancer);
        void setVariables(bool lungCancer);

        size_t cost(double diameter) const;

                                                            // for LungCancer
        double lcTreatment(double age, double diameter,         // @ treatment
                           bool metastasis) const;

        double otherTreatment(double age, double diameter,      // @ treatment
                           bool metastasis) const;

        double discount(double age, size_t cost) const;     // otherwise
};

#include "costs.f"

#endif
