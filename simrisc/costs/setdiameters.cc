#define XERR
#include "costs.ih"

void Costs::setDiameters()
{
    d_base.back() = "diameters:";

    if (not extractDiameters(Parser::one(d_base)))
        Err::specification();
}
