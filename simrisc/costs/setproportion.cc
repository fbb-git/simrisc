#define XERR
#include "costs.ih"

void Costs::setProportion(bool lungCancer)
{
    d_base.back() = "proportion:";

    LineInfo const *lineInfo = Parser::one(d_base).get();
    istringstream in{ lineInfo->tail };            // get the line's tail

    if (not (in >> d_discountProportion))
    {
        Err::msg(Err::SPEC_ERROR) << 
            "[Costs:] proportion: must specify at least one value" << endl;
        return;
    }
                                                    // at LC simulations:
    if (lungCancer)                                 // maybe a 2nd proportion?
    {
        double prop2;
        if (in >> prop2)                            // if so, use it 
            d_discountProportion = prop2;           
    }

    if (not Globals::proportion(d_discountProportion))
        Err::range();
}
