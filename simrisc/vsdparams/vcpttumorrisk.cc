//#define XERR
#include "vsdparams.ih"

// see calcTumorRisk in SimRisc.cpp

// override
void VSDParams::v_cptTumorRisk(DoubleVect &ageValues)
{
    size_t age = 0;
    double mean = d_vsd[MEAN].value();
    double stdDev = d_vsd[SDEV].value();

    double factor1 = d_vsd[RISK].value() / (stdDev * Globals::s_sqrt2PI);

    for (double &risk: ageValues)          // compute the risks per age value
    {
        double factor2 = (age++ - mean) / stdDev;
        risk = factor1 * exp(-(factor2 * factor2 / 2));
    }
}
