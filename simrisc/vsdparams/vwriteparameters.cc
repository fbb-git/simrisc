#define XERR
#include "vsdparams.ih"

// override
void VSDParams::v_writeParameters(ostream &out) const
{
    Globals::setPrecision(out, 2) <<
           setw(8) << ' ' << "std. dev.:    " << d_vsd[SDEV] << '\n';

    VSD::fmt(2, 3, 0, 4);
    out << setw(8) << ' ' << "lifetimeRisk: " << d_vsd[RISK] << '\n';

    VSD::fmt(2, 1, 3, 3);
    out << setw(8) << ' ' << "mean age:     " << d_vsd[MEAN] << "\n\n";
}
