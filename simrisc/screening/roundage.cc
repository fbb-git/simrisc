//#define XERR
#include "screening.ih"

double Screening::roundAge(size_t idx) const
{
    double ret = d_roundVect[idx].age();
    return ret;
}
