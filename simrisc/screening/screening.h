#ifndef INCLUDED_SCREENING_
#define INCLUDED_SCREENING_

// Screening:
//
//     # round: age    space separated modalities.
//     round:     50  Mammo
//     round:     52  Mammo
//     round:     54  Mammo
//     round:     56  Mammo
//     round:     58  Mammo
//     round:     60  Mammo
//     round:     62  Mammo
//     round:     64  Mammo
//     round:     66  Mammo
//     round:     68  Mammo
//     round:     70  Mammo
//     round:     72  Mammo
//     round:     74  Mammo

//                              alternatively:
//      round:  none

//
//     # screening         value
//     attendanceRate:     .8
//

#include <iosfwd>

#include "../round/round.h"

class Modalities;

class Screening
{
    Modalities &d_modalities;

    StringVect d_base;
                                            // this vector's size holds the
    RoundVect d_roundVect;                  // #rounds
    BoolVect d_attend;                      // true: case attends the round

                                            // attendance rate, configured at
    double d_rate;                          // Screening: attendanceRate:

    // points to static members:                        // errRisk1404 or
    void (*d_errRiskVector)(DoubleVect &,               // errRiskA1405
                            Modalities const &,
                            Round const &, uint16_t, double, double);

    bool d_lungCancer;

    public:
        Screening(Modalities &modalities);

        double rate() const;                                            // .f

        DoubleVect radiationRisk(           // 24/1 also sets d_attend's values
                            Modalities const &modalities,
                            Uint16Vect const &indices,
                            double beta, double eta);     // beir7 values

        DoubleVect ages() const;  // vector of ages of the screening rounds

        uint16_t nRounds() const;                                       // .f
        double roundAge(size_t idx) const;          // age at round idx    .f

                                                    // roundIdx is, e.g.
        Round const &round(size_t roundIdx) const;  // Loop::d_round       .f

        void writeParameters(std::ostream &out) const;

        bool attend(size_t idx) const;  // 24/1 true: case attended this round

    private:
        bool addModality(std::istream &in, Round &round);
                                            // false: round: none, noneLine
                                            // has the last noneLine
        bool addRound(LineInfo *noneLine, LineInfo const &line);

        bool increasingAge(Round const &round) const;

        static void errRisk1404(DoubleVect &riskVect,
                        Modalities const &modalities,
                        Round const &round, uint16_t biRadIdx,
                        double beta, double eta);

        static void errRisk1405(DoubleVect &riskVect,

                        Modalities const &modalities,
                        Round const &round, uint16_t biRadIdx,
                        double beta, double eta);

                                            // called by errRiskRelative
        static double beir7Err(Round const &round, uint16_t idx, double beta,
                               double eta, Modalities const &modalities);

                                            // called by earRisk
        static double beir7Err(uint16_t age, Round const &round,
                               uint16_t idx, double beta, double eta,
                               Modalities const &modalities);

        void setAttendanceRate();

        void setRounds();

//        bool warnCT(std::string const &modality);
//        bool warnedCT(std::string const &modality);
//        bool warnNotCT(std::string const &modality);
//        bool warnedNotCT(std::string const &modality);
};

#include "screening.f"

//                                                // CT for lung cancer,
//                                                // not CT for breast cancer
//    bool (Screening::*d_checkCT)(std::string const &modality);

//
//        void beir7dose(double const *dose);                         //  .h
//
//
//
//
//        RoundVect const &rounds() const;
//
//
//        StringSet const &modalities() const;


//     double const *d_beir7dose;      // set from Screening using
//                                     // beir7dose()
//
//     RoundVect d_rounds;
//     StringSet d_modalities;         // specified modalities

//        void set(Distribution *dest,                    // 1
//                 Scenario const &scenario, char const *id);

//                                                        // 2
//        static bool set(Distribution *dest,
//                         Scenario::const_iterator const &iter);
//

//

// inline StringSet const &Screening::modalities() const
// {
//     return d_modalities;
// }
//
// inline RoundVect const &Screening::rounds() const
// {
//     return d_rounds;
// }
//
// inline void Screening::beir7dose(double const *dose)
// {
//     d_beir7dose = dose;
// }

////////////////////////////////////////////////////////////
// inline double Screening::Distribution::value() const
// {
//     return d_value;
// }

//     static char const s_errorID[];
//     static char const s_attendanceID[];
//     static char const s_roundID[];


#endif
