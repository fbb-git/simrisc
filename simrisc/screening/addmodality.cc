#define XERR
#include "screening.ih"

bool Screening::addModality(istream &in, Round &round)
{
    string modality;                        // determine the modalities to use

    while (in >> modality)
    {
        Err::Context context;

        if (uint16_t idx = d_modalities.find(modality); idx == UNDEFINED)
            context = Err::UNDEFINED_MODALITY;


            // -------------------------------------------
            // lungCancer      CT      action
            // -------------------------------------------
            //     0           0       add
            //     0           1       BC_CT_INVALID
            //     1           0       LC_INVALID_MODALITY
            //     1           1       add
            // -------------------------------------------
                                            // if LC simulation is requested
                                            // but no CT modality requested
        else if (d_lungCancer and modality != "CT")
        {
            Err::msg(Err::LC_INVALID_MODALITY) << modality << endl;
            return false;
        }
                                            // BC simulation and CT is spec'd
        else if (not d_lungCancer and modality == "CT")
            context = Err::BC_CT_INVALID;
        else
        {
            // TODO: the last entry in d_roundVect (may still be empty)
            //       may not contain modality 'idx'
            if (round.add(idx))
            {
                d_modalities.activate(modality);
                continue;
            }

            context = Err::MODALITY_REPEATED;
        }

        Err::msg(context) << modality << endl;
        return false;

    }

    return true;
}

//        if ((this->*d_checkCT)(modality))   // issue one warning if CT is
//            continue;                       // not specified
