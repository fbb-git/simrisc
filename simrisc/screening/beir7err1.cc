//#define XERR
#include "screening.ih"

// called from (deprecated) errRisk1404.cc
// index is the biRad column index to use with BC
// and 0 when used with LC, as LC has no dose variation.

// static
double Screening::beir7Err(Round const &round, uint16_t index, double beta,
                           double eta, Modalities const &modalities)
{
    double risk = 1;
    double factor = beta * pow(round.age() / 60, eta) / 1000;

                            // for modalities not defining 'dose' (MRI)
                            // 0 is returned
    for (ModBase const *modBase: modalities.use(round.modalityIndices()))
        risk *= (1 + factor * modBase->dose(index));

    return risk;
}
