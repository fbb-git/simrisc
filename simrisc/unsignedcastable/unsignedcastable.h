#ifndef INCLUDED_UNSIGNEDCASTABLE_
#define INCLUDED_UNSIGNEDCASTABLE_

    // concept used by Parser's unsignedcast templates to ensure that
    // the arguments can be static_cast to an unsigned

template <typename Type>
concept UnsignedCastable =
    requires(Type value)
    {
        static_cast<unsigned>(value);
    };

#endif
