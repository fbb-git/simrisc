//#define XERR
#include "agegroupvsd.ih"

// static
ostream &AgeGroupVSD::insert(ostream &out, AgeGroupVSDvect const &vect)
{
    for (unsigned idx = 0, end = vect.size(); idx != end; ++idx)
        out << setw(s_indent) << ' ' << "ageGroup: " << vect[idx] << '\n';

    return out;
}
