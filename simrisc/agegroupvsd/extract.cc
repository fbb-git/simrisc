#define XERR
#include "agegroupvsd.ih"

    // e.g.,    1 - 50   .61    4.38   .43    Normal
    // or:      1 - 50          4.38   .43    Normal
    //
    // or (lung):        .21    4.59   .74    Normal

std::istream &AgeGroupVSD::extract(std::istream &in)
{
    if (d_extractGroup)
        in >> d_group;

    if (d_stdDev < 0)
    {
        string value;
        in >> value;
        istringstream in{ value };
        in >> d_vsd;
    }
    else
    {
        in >> d_stdDev >> d_vsd;

        // the default Distribution setting is VARY_OK: OK for AgeGroupVSD

        if (d_stdDev < 0)
            Err::msg(Err::NEGATIVE) << "(final std. dev.)" << endl;
    }

    return in;
}
