//#define XERR
#include "agegroupvsd.ih"

// static
void AgeGroupVSD::fmt(unsigned indent,
                      unsigned mIntWidth, unsigned mPrec,
                      unsigned sdIntWidth, unsigned sdPrec)
{
    s_indent = indent;

    VSD::fmt(mIntWidth, mPrec, sdIntWidth, sdPrec);
}
