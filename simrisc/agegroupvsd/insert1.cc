//#define XERR
#include "agegroupvsd.ih"

std::ostream &AgeGroupVSD::insert(std::ostream &out) const
{
    out << d_group << ", ";              // insert the age group

    if (d_stdDev >= 0)                      // then the sd.dev (if available)
        Globals::setPrecision(out, 3) << "std.dev: " << 
            (d_exp ? exp(d_stdDev) : d_stdDev) << ";    ";

    return out << d_vsd;                    // then the VSD
}
