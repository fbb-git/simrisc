//#define XERR
#include "agegroupvsd.ih"

void AgeGroupVSD::ln()
{
    d_vsd.ln();
    if (d_stdDev > 0)
        d_stdDev = log(d_stdDev);
    d_exp = true;
}
