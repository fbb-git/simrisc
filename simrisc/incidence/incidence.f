inline DoubleVect const &Incidence::tumorRisk(size_t idx) const
{
    return d_tumorRisk[idx];
}
