//#define XERR
#include "incidence.ih"

void Incidence::cptTumorRisk()
{
                                        // visit all incidence modes
    for (size_t idx = 0, end = d_params.size(); idx != end; ++idx)
                                        // compute the tumorrisks per age
        d_params[idx]->cptTumorRisk(d_tumorRisk[idx]);
}
