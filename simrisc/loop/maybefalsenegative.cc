#define XERR
#include "loop.ih"

extern size_t g_caseIdx;

void Loop::maybeFalseNegative(ModBase *modBase, double screeningAge)
{
    d_tumor.characteristicsAt(screeningAge);

    if (not falseNegative(modBase))
        tumorDetected(modBase, screeningAge);
}


// see also: Marcel: Tue, 9 Jun 2020 13:01:00 +0000
//           Marcel: Thu, 11 Jun 2020 10:35:44 +0000 (MRI, Mammo, Tomo are
//                   all used

// the check for tumor.diameter() >= 1 is omitted, since
// all tumors have at least 1 mm diameter
// (see Marcel: Tue, 30 Jun 2020 10:46:05 +0000)
