#define XERR
#include "loop.ih"

    // perform a screening

void Loop::screen(double screeningAge)
{
    // visit all modalities: for (ModBase *...) or comparable

    g_log << __FILE__ " performing screening at age " << screeningAge << '\n';

                                            // org/screen: 4, screen.txt: 1
    for (ModBase *modBase: d_modalities.use(
                                d_screening.round(d_round).modalityIndices())
    )
    {
        if (not use(modBase))
            continue;
                                            // adds the basic costs: screening
                                            // for lungcancer, 'costs:' for
                                            // other modalities
        double cost = d_costs.screening(screeningAge, modBase->cost());
        d_caseCost += cost;
        g_log << "Screening costs, round " << (d_round + 1) << 
                ", case " << (g_caseIdx + 1) << 
                ": cost: " << cost <<
                ", old sum: " << d_roundCost[d_round] <<
                ", new sum: ";
        d_roundCost[d_round] += cost;
        g_log <<  d_roundCost[d_round] << '\n';

        if (not d_tumor.at(screeningAge))   // no tumor, but screening may
            maybeFalsePositive(modBase, screeningAge);  // detect a tumor

        else                                // there is a tumor which may not
            maybeFalseNegative(modBase, screeningAge);          // be detected
    }

    g_log << __FILE__ " completed screening at age " << screeningAge << '\n';
}



//
//                                            // determine the tumor
//                                            // characteristics.
//        if (d_tumor.selfDetectable())
//            d_tumor.characteristicsAt(d_tumor.selfDetectAge()); //screeningAge);
//
//                        // Org. 2nd term: screeningAge >= d_tumor.onset())
//                        // test2 (see below)
//        if (d_tumor.selfDetectable() and d_tumor.onset() <= screeningAge)
//            maybeDetect(modBase, screeningAge);
//        else
//            maybeFalsePositive(modBase, screeningAge);

// tumor   test2   selfDetectabledetect selfDetectablefalsepos
//   1       1         1           0
//   1       0         0           1
//   0       1         0           1
//   0       0         0           1
//
// -> calling selfDetectableFalsePositive may not be called when just 'not d_tumor'
//    is true.



// Sensitivity (true positive rate) refers to the probability of a positive
// test, conditioned on truly being positive (so: the power)
//
// Specificity (true negative rate) refers to the probability of a negative
// test, conditioned on truly being negative. (so: 1 - alpha)


//xerr("");
//for (auto const &id: d_screening.round(d_round).modalityIDs())
//cerr<<"     id: " << id <<'\n';


//xerr("use " << modBase->id());

//xerr("cpt costs");
