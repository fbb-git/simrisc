#define XERR
#include "loop.ih"

double Loop::sensitivity(ModBase *modBase) const
{
                // NOTE: d_indices uses indices (0..4) instead of numbers
                //       (as used in the original code)
    return modBase->sensitivity(d_indices[d_round]);
}

////Calculate sensitivity for the modality and breastdensity (possibility to make function of age and tumorsize in the future but not used for now)
//float calcSens(float age, float tumorsize, int modality, int density ) {
//    switch (modality) {
//        case 0: //Mammography
//            return sensMam[density-1];
//        case 1: //Tomo
//            return sensTomo[density-1];
//    }
//}
//
