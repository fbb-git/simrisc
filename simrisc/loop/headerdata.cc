//#define XERR
#include "loop.ih"


//  woman, death cause, death age, tumor present, tumor detected, tumor is
// interval, tumor diameter, tumor self-detect age, tumor death age, tumor
// doubling time, tumor onset age, tumor preclinical period, natural death age,
// death status, costs, self detected, round detected, screening rounds, TNM


CSVTable Loop::headerData(size_t iter) const
{
    CSVTable tab{ outStream(d_options.dataFile(), iter), "  " };

    if (not tab.stream())           // tab.stream(): the stream to write the
        return tab;                 //      data to.

    labels(tab.stream());

    ostream &out = tab.stream();    // write the status legenda
    out << "exit status legend:\n";
    for (size_t status = NATURAL_PRE; status != Status_END; ++status)
        out << setw(5) << status << ": " << s_status[status] << '\n';

        // the fmt() insertions define the widths of the columns, 
        // not their header labels. Blanks are ignored, hence 'costs67890'
        // case  cause  age  natural  status  present  detected  interval
        // 1     2      3    4        4       6        7         8
        // diameter days  period  onset  self-detect  death
        // 9        10    11      12     13           14
        // costs  self  round  rounds  TNM
        // 15     16    17     18      19 
        //              **

    tab.fmt() <<  right(7) <<                                   //  1
                  "Natural" << right("100.12", 2) <<            //  2 -  3
                  "natural" << "status" <<                      //  4 -  5
                  "present" << "detected" << "interval" <<      //  6 -  8
                  "diameter" << "doubling" << "preclinical" <<  //  9 - 11

                                           //  death:
                  "onset" << "self-detect" << "100.00" <<       // 12 - 14
                "costs67890" <<                                 // 15
                "biop567" << "self" << "round" <<               // 15-pre - 17
                    right(d_nRounds > 10 ? d_nRounds : 10) <<   // 18
                    right(5);                                   // 19

    tab << hline();
                                                                // 6 - 14 + hline
    tab.row(5) << join(9, FMT::CENTER) << "tumor";
    tab.row(5) << hline(9);

    tab.row(1) <<                                               //  1
            join(4, FMT::CENTER) << "death" <<                  //  2 -  5
            join(6, FMT::RIGHT) << ' ' <<                       //  6 -  11
            ' ' <<                                              // 12
            join(2, FMT::CENTER) << "age" <<                    // 13 - 14
            join(2, FMT::CENTER) << "costs" <<                  // 15-pre 15
            join(2, FMT::CENTER) << "detected";                 // 16 - 17


    tab.row(1) <<                                               //  1
            hline(4) <<                                         //  2 -  5
            join(4, FMT::RIGHT) << ' ' <<                       //  6 -  9
            "doubling" << "preclinical" << ' ' <<               // 10 - 12
            hline(2) <<                                         // 13 - 14
            hline(2) <<                                         // 15-pre 15
            hline(2) << "screening";                            // 16 - 18

    tab << "case" <<                                            //  1
            "cause" << "age" <<                                 //  2 -  3
            "natural" << "status" <<                            //  4 -  5
            "present" << "detected" << "interval" <<            //  6 -  8
            "diameter" << "days" << "period" <<                 //  9 - 11
            "onset" << "self-detect" << "death" <<              // 12 - 14
            "screening" <<                                      // 15
            "biop" <<                                       // 15-pre
            "self" << "round" << "rounds" <<  "TNM";            // 16 - 19


    tab << hline();

    tab.sep(", ");

    return tab;
}

//         out <<
// 1           " woman, "
// 2           "death cause, "
// 3           "death age, "
// 4           "tumor present, "
// 5           "tumor detected, "
// 6           "tumor is interval, "
// 7           "tumor diameter, "

// 8  11       "tumor self-detect age, "
// 9  12       "tumor death age, "
// 10 8        "tumor doubling time, "
// 11 10       "tumor onset age, "
// 12 9        "tumor preclinical period, "

// 13          "natural death age, "
// 14          "death status, "
// 15          "costs, "
// 16          "self detected, "
// 17          "round detected, "
// 18          "screening rounds, "
// 19          "TNM, "
//             "\n";
//     }
