//#define XERR
#include "loop.ih"

void Loop::tumorDetected(ModBase *modBase, double screeningAge)
{
                // Found a tumor during the screening
    d_tumor.found();
    ++d_nDetections[d_round];

    d_caseCost += treatmentCosts(screeningAge);
    addBiopCosts(d_costs.usingAt(screeningAge));

    d_tumor.setDeathAge(screeningAge);
    d_deathAge = min(d_tumor.deathAge(), d_naturalDeathAge);

    d_roundDetected = d_round + 1;

    left(SCREEN_DETECTED, d_deathAge);
}
