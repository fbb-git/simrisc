#include <cmath>
#include <iostream>

using namespace std;

int main()
{
    while (true)
    {
        cout << "Ncases? ";

        unsigned nCases;
        cin >> nCases;

        unsigned nThreads = round(nCases / 10000.);

        if (nThreads == 0)
            nThreads = 1;
        else if (nThreads > 5)
            nThreads = 5;

        double fraction = nCases / static_cast<double>(nThreads);

        for (unsigned idx = 0, sum = 0; idx != nThreads; ++idx)
        {
            unsigned total = round((idx + 1) * fraction);
            cout << "thread " << idx << ": ncases: " << (total - sum) <<
                                        ", total = " << total << '\n';
            sum = total;
        }
    }
}
