//#define XERR
#include "loop.ih"

double Loop::naturalDeathAge()
{
    double rnd;
    double ret = findAge(                       // Globals::
                        d_scenario.cumDeathProp(),
                        rnd = Random::instance().uniformCase()
                    );

    double age =  ret < MAX_AGE ? ret : MAX_AGE;

    g_log << "  Loop::naturalDeathAge: locate " << rnd <<
                                                    " in cum.death probs\n" <<
             "    natural death age: " << age << '\n';

    return age;
}
