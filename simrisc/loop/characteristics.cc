// #define XERR
// #include "loop.ih"
// 
// // treatmentDeath(): characteristics(LEFT_POST, TUMOR_POST);
// 
// extern size_t g_caseIdx;
// 
// void Loop::characteristics(Status natural, Status tumor)
// {
//                                 // the tumor was self-detected: determine
//                                 // its characteristics
//     d_tumor.characteristicsAt(d_tumor.selfDetectAge());  
// 
//     d_selfDetected = true;
// 
//         // treat the tumor: add the treatment cost and compute the age of
//         // tumor-caused death. There is a tumor: no need to call
//         // diameterCheck()
//     d_caseCost += treatmentCosts(d_tumor.selfDetectAge());
// 
//     d_tumor.setDeathAge();
// 
//     if (d_naturalDeathAge < d_tumor.deathAge())     // naturally caused death
//         setStatus(natural, d_naturalDeathAge);
//     else                                            // or tumor caused death
//         setStatus(tumor, d_tumor.deathAge());
// }
// 
// 
// 
// // double cost = treatmentCosts(d_tumor.selfDetectAge());
// // xerr("costs: " << cost << ", age: " << d_tumor.selfDetectAge() << ", dia: " <<[bsl]
// // d_tumor.diameter());
// // d_caseCost += cost;
// 
