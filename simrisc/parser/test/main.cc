//#define XERR
#include "main.ih"

// needs parser, configlines and err

void show(char const *label, Parser::Lines lines)
{
    cout << label << ":\n";
    while (true)
    {
        auto const *element = lines.get();
        if (element == 0)
            break;

        cout << "src: " << element->src << ", nr: " << element->lineNr <<
                ", tail (value(s)): " << element->tail << '\n';
    }
}

int main(int argc, char **argv)
try
{
    if (argc == 1)
        throw Exception{} << "arg1: simrisc conf., arg2: one analysis spec\n";

    Parser parser;

    StringVect labels;
    Parser::OptionsVect options;

    ifstream in{ argv[2] };
    parser.load(in, 1, labels, options);
    parser.load(argv[1]);

    cout << "Labels:\n";
    for (string const &label: labels)
        cout << "   Label: " << label << '\n';

    cout << "Options:\n";
    for (auto const &option: options)
        cout << "   Option: `" << option.name << "': " <<
                                 option.value << '\n';

    show("Scenario, spread",    parser.any({"Scenario:", "spread:"}));
    show("Scenario, cases",     parser.any({"Scenario:", "cases:"}));
    show("Survival, type",      parser.any({"Tumor:", "Survival:", "type:"}));
    show("Survival, lung1",     parser.any({"Tumor:", "Survival:", "lung1:"}));
    show("Costs, diameters",    parser.any({"Costs:", "diameters:"}));
}
catch(int x)
{
    return x;
}
catch (exception const &exc)
{
    cerr << exc.what() << '\n';
}
catch (...)
{
    cerr << "unexpected exception\n";
    return 1;
}
