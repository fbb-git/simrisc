//#define XERR
#include "parser.ih"

Parser::Lines::Lines(LineInfoVect::const_iterator const &begin,
                            LineInfoVect::const_iterator const &end)
:
    d_size(end - begin),
    d_iter(begin),
    d_end(end)
{
    Err::reset(*d_iter);
}
