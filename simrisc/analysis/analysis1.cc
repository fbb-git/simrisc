#define XERR
#include "analysis.ih"

    // the error count is reset by the Simulator

Analysis::Analysis(std::istream &&stream, uint16_t lineNr)
{
    Parser::OptionsVect optionsVect;
                                                    // read the analysis specs
    d_parser.load(stream, lineNr, d_labels, optionsVect);

                                                    // set the options used
    actualize(optionsVect);                         // for this analysis

    d_parser.load(Options::instance().configFile());  // read the config file
}
