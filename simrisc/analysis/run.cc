#define XERR
#include "analysis.ih"

//extern Parser *pPtr;

void Analysis::run()
{
    requireBase();              // the base directory must be (made) available

    Loop loop{ d_labels };

                                // errors may be set at Options, ConfFile
    if (emsg.count() != 0)      // construction, or Loop construction
        return;

    loop.iterate();
}
