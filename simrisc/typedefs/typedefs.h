#ifndef INCLUDED_TYPEDEFS_
#define INCLUDED_TYPEDEFS_

#include <vector>
#include <cstdint>
#include <string>
#include <tuple>
#include <unordered_set>

#include "../enums/enums.h"

//struct
struct LineInfo
{
    ParamsSrc   src;
    uint16_t    lineNr;
    std::string txt;
    std::string tail;
};
//=

//types
using SizePair      = std::pair<size_t, size_t>;
using DoublePair    = std::pair<double, double>;

using DoublesStringTuple = std::tuple<double, double, std::string>;
using SizesDoubleTuple  = std::tuple<size_t, size_t, double>;
using StringSet     = std::unordered_set<std::string>;

using BoolVect      = std::vector<bool>;
using DoubleVect    = std::vector<double>;
using DoubleVect2   = std::vector<DoubleVect>;
using Double2Vect   = std::vector<DoublePair>;
using LineInfoVect  = std::vector<LineInfo>;
using SizeVect      = std::vector<size_t>;
using StringVect    = std::vector<std::string>;
using Uint16Vect    = std::vector<uint16_t>;
using SizesDoubleVect  = std::vector<SizesDoubleTuple>;

using SizeStrVectPair   = std::pair<size_t, StringVect>;
using SizeStrVect2  = std::vector<SizeStrVectPair>;

using RowCol        = std::pair<uint16_t, uint16_t>;

//=

#endif


