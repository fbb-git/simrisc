//#define XERR
#include "distribution.ih"

std::ostream &Distribution::insert(std::ostream &out) const
{
    switch (d_distType)
    {
        case N_DISTRIBUTIONS:
            out << setw(12) << ' ';
        break;

        case BETA_VARY:
        {
            out << '\n' <<
                setw(15) << ' ' <<
                            "Beta:  constant  "
                                   "  factor  "
                                   "  aParam  "
                                   "  bParam\n" <<
                setw(20) << ' ';

            auto pair = Random::instance().betaParameters();
            Globals::setPrecision(out, 5);
            for (size_t idx = 0, end = pair.first; idx != end; ++idx)
                out << setw(10) << pair.second[idx];
        }
        break;

        default:
            Globals::setWidthPrec(out, s_width, s_precision) <<
                (d_exp ? exp(d_confValue) : d_confValue) <<
                    " (" << s_name[d_distType] << ')';
        break;
    }

    return out;
}
