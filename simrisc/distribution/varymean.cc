#define XERR
#include "distribution.ih"

// in the original code refreshing is handled by reloading the complete config
// file. So the argument passed to this member should be the initial value
// and not the updated value.

double Distribution::varyMean(double orgValue) const
{
    Random &random = Random::instance();

    switch (d_distType)
    {
        case NORMAL_VARY:
        return orgValue + random.normalVary() * d_confValue;

        case UNIFORM_VARY:
        return orgValue + d_confValue * (random.uniformVary() - .5);

        case LOGNORMAL_VARY:
        return random.logNormalVary(orgValue, d_confValue);

        default:
        return orgValue;
    }
}

//        case EXPONENTIAL:
//        return orgValue + random.exponential(orgValue);
