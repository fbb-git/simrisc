//#define XERR
#include "distribution.ih"

// Beta distribution is handled by extract()

void Distribution::prepareVary(istream &in)
{
    if (not Globals::vary())                // no parameter spreading is used
        return;

    switch (d_distType)
    {
        case NORMAL_VARY:
        break;

        case UNIFORM:
            d_distType = UNIFORM_VARY;
        break;

        case LOGNORMAL:
            d_distType = LOGNORMAL_VARY;
        break;

        default:
        return;
    }
}
