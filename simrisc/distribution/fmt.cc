//#define XERR
#include "distribution.ih"

// static
void Distribution::fmt(unsigned intWidth, unsigned precision)
{
    s_width = intWidth + precision + (precision > 0);
    s_precision = precision;
}
