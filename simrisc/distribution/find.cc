#define XERR
#include "distribution.ih"

// static
DistType Distribution::find(string const &name)
{
    return static_cast<DistType>(
                std::find(s_name.begin(), s_name.end(), name) - s_name.begin()
            );
}
