#define XERR
#include "tableparams.ih"

void TableParams::checkIncreasing()
{
                                            // check increasing ages
    DoublePair saw{ 0, 0 };                 // and increasing probabilities

    for (DoublePair &pair: Ranger(d_riskTable.begin() + 1, d_riskTable.end()))
    {
        if (pair.first <= saw.first or pair.second < saw.second)
            throw "ages and probabilities must increase";

        saw = pair;
    }
}
