//#define XERR
#include "tableparams.ih"

// overrides
void TableParams::v_vary(ostream &out)
{
    out << "    " <<
                (
                    not Globals::isBreast() ?
                        Globals::simTypeLabel(LC)
                    :
                        label()
                ) << "\n"
           "      tableParams:   no spread\n";
}
