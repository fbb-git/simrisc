#define XERR
#include "tableparams.ih"

bool TableParams::appendPair(string const &line)
{
    size_t age;
    double prob;

    istringstream in{ line };
    while (in >> age)
    {
        if (not (in >> prob))       // 2nd pair value not available
        {
            d_error = true;
            Err::specification();
            return false;
        }
        d_riskTable.push_back(DoublePair{ age, prob });
    }
    return true;                    // all pairs were read from line
}
