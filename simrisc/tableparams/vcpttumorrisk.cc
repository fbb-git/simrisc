//#define XERR
#include "tableparams.ih"

// virtual
void TableParams::v_cptTumorRisk(DoubleVect &ageValues)
{
    DoublePair const *from = &d_riskTable.front();
                                                // assign the 1st prob.:
    ageValues.front() = from->second;           // cum.prob for age 0

    double *begin = &ageValues.front() + 1;     // next value to assign

                                                // visit all pairs of
                                                // riskTable elems
    for (size_t idx = 1, end = d_riskTable.size(); idx != end; ++idx)
    {
        DoublePair const *to = &d_riskTable[idx]; // eventually the last elem.

                                                // points beyond the ageValues
        double *beyond = &ageValues[            // elem. beyond to's index
                            static_cast<size_t>(to->first + 1.5) ];

        fill(begin, beyond,                     // assign [begin..beyond)
             (to->second - from->second) / (to->first - from->first)
        );

        begin = beyond;                         // the begin of the next range
        from = to;
    }
}

// void TableParams::v_cptTumorRisk(DoubleVect &ageValues)
// {
//     DoublePair from = &d_riskTable.front();
//                                     // visit pairs of d_simriskTable elements
//                                     // (elements idx and idx + 1)
//     for (size_t idx = 0, end = d_riskTable.size() - 1; idx != end; ++idx)
//     {
//         DoublePair to = d_riskTable[idx + 1];
// 
//         to.second -= d_riskTable[idx].second;   // convert cumprobs to probs
// 
//         for (                       // set the age-range [age..endAge)
//             size_t age = from->first + .5, endAge = to.first + .5;
//                 age != endAge;
//                     ++age
//         )
//             ageValues[age] = linear(age, from, to);
// 
//         from = to;
//     }
// 
//     ageValues.back() = d_riskTable.back().second - from.second;
// }
