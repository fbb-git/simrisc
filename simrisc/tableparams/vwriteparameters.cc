#define XERR
#include "tableparams.ih"

// override
void TableParams::v_writeParameters(ostream &out) const
{
    Globals::setPrecision(out, 3) <<
           setw(8) << ' ' << "riskTable:\n" << 
           setw(9) << ' ';

    for (size_t idx = 0, end = d_riskTable.size(); idx != end; )
    {
        out << setw(3) << static_cast<size_t>(d_riskTable[idx].first + .5) << 
                ": " << d_riskTable[idx].second;

        if (++idx == end)
        {
            out << "\n\n";
            return;
        }
        if (idx % 6 != 0)
            out << ", ";
        else
            out << '\n' << setw(9) << ' ';
    }
}
