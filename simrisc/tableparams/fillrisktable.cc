#define XERR
#include "tableparams.ih"

void TableParams::fillRiskTable(StringVect &base)
{
    base.back() = "riskTable:";

    Parser::Lines lines = Parser::any(base);

    DoublePair next;
    LineInfo const *ptr;
    do
        ptr = lines.get();
    while (ptr != 0 and appendPair(ptr->tail));

    checkTable();       // ensure values are incrementing and complete
}
