//#define XERR
#include "options.ih"

void Options::conflictCheck() const
{
    size_t count = (d_specified[ANALYSIS].find('a') != string::npos) +
                   (d_specified[ANALYSIS].find('t') != string::npos);

    if (count == 1)
        throw Exception{} << "Options --death-age and --tumor-age "
                "must both be specified";

    if (count != 0 and d_specified[ANALYSIS].find('c') != string::npos)
        throw Exception{} << "--case and --death_age are mutually exclusive";
}
