#define XERR
#include "options.ih"

Options::Options()
:
    d_arg(Arg::instance()),
    d_naturalDeathAge(END_AGE),
    d_tumorAge(END_AGE),
    d_lastCase(0)
{
    imsg.off();

    if (char *cp = getenv("HOME"); cp)
        d_home = cp;

    if (d_arg.option('V'))
    {
        d_specified[STARTUP] += 'V';
        imsg.on();
    }

    configOption();                         // set d_config[STARTUP]
    baseOption();

    fileOption(d_dataFile, 'D');
    fileOption(d_roundsFile, 'R');
    fileOption(d_sensitivityFile, 'S');
    fileOption(d_spreadFile, 's');
    fileOption(d_parametersFile, 'P');

    string value;
    if (d_arg.option(&value, 'a'))
        deathAgeOption(value);

    if (d_arg.option(&value, 'd'))
        cumDeathOption(value);

    if (d_arg.option(&value, 't'))
        tumorAgeOption(value);

    if (d_arg.option(&value, 'l'))
        lastCaseOption(value);

    if (d_arg.option(&value, "log"))
        logOption(value);

    setSimulationType();                // 'c'
}
