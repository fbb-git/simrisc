#define XERR
#include "options.ih"


void Options::actualize()                       // transform ~ and +
{
    d_specificAges = d_specified[STARTUP].find("at") != string::npos;

    replaceHome(d_config[ANALYSIS]);
    replaceHome(d_cumDeath);

    setBase();

    replacePlus(d_cumDeath);
    replacePlus(d_dataFile[ANALYSIS]);
    replacePlus(d_parametersFile[ANALYSIS]);
    replacePlus(d_roundsFile[ANALYSIS]);
    replacePlus(d_sensitivityFile[ANALYSIS]);
    replacePlus(d_spreadFile[ANALYSIS]);


    Globals::setSimType(d_cancer[ANALYSIS]);  // set the cancer type

    conflictCheck();                          // prevent conflicting options
}
