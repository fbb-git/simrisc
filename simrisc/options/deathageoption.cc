//#define XERR
#include "options.ih"

void Options::deathAgeOption(string const &value)
{
    d_specified[STARTUP] += 'a';
    deathAge(value);
}
