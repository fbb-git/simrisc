//#define XERR
#include "options.ih"

void Options::baseOption()
{
    string value;

    if (d_arg.option(&value, 'B'))          // get the option's value
        d_specified[STARTUP] += 'B';
    else
        value = s_base;                     // or use the default

    d_base[STARTUP] = value;

    imsg << "using base directory `" << value << "'\n";
}
