//#define XERR
#include "options.ih"

Options *Options::s_options;

char const Options::s_base[] = "./";
char const Options::s_config[] = "~/.config/simrisc";

unordered_map<int, char const *> Options::s_fileName
{
    { 'D', "+data-$.txt"        },
    { 'P', ""                   },
    { 'R', "+rounds-$.txt"      },
    { 'S', "+sensitivity.txt"   },
    { 's', "+spread-$.txt"      },
};
