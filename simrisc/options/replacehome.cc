//#define XERR
#include "options.ih"

void Options::replaceHome(string &path)
{
    static bool warned = false;

    if (not path.empty() and path.front() == '~')
        path.replace(0, 1, d_home);

    if (d_home.empty() and not warned)
    {
        warned = true;
        wmsg << "No HOME environment variable found\n";
    }
}
