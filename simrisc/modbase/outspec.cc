//#define XERR
#include "modbase.ih"

// static
void ModBase::outSpec(ostream &out, char const *prefix, unsigned fill,
                                    AgeGroupVSD const &spec)
{
    out << prefix << setw(fill) << ' ' <<
                     setw(2) << spec.beginAge() << " - " << setw(2);

    if (Globals::isZero(spec.endAge() - END_AGE))
        out << '*';
    else
        out  << spec.endAge();

    out << ':'  << setw(6) << spec.value() << ";\n";
}
