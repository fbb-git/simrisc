inline StringVect &ModBase::base()
{
    return d_base;
}

inline size_t ModBase::cost() const
{
    return d_cost;
}

inline void ModBase::count(size_t round)
{
    ++d_count[round];
}

inline bool ModBase::defined() const
{
    return d_defined;
}

inline void ModBase::falsePositive()
{
    ++d_falsePositives;
}

inline std::string const &ModBase::id() const
{
    return d_id;
}

inline double ModBase::dose(uint16_t idx) const
{
    return vDose(idx);
}

inline size_t ModBase::operator[](size_t idx) const
{
    return d_count[idx];
}

inline double ModBase::sensitivity(size_t idx) const
{
    return vSensitivity(idx);
}

inline double ModBase::specificity(double age) const
{
    return vSpecificity(age);
}

inline void ModBase::vary(std::ostream &out)
{
    return vVary(out);
}

