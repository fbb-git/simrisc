//#define XERR
#include "mri.ih"

// override
void MRI::vVary(ostream &out)
{
    d_sensitivity.vary();
    d_specificity.vary();

    out << "  MRI:\n"
           "    sensitivity:  ";
    d_sensitivity.showVary(out);

    out << "    specificity:  ";
    d_specificity.showVary(out);
}
