#ifndef INCLUDED_HASAGEGROUP_
#define INCLUDED_HASAGEGROUP_

    // concept used by Group to ensure that
    // Type has a member group() returning a const & to a Group

class Group;

template <typename Type>
concept HasGroup =
    requires(std::vector<Type> const &object)
    {
           { object.back().group() } -> std::same_as<Group const &>;
    };

#endif
