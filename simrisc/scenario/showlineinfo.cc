//#define XERR
#include "scenario.ih"

// static
LineInfo const *Scenario::showLineInfo(LineInfo const *ptr)
{
    if (ptr != 0)
        cout << "[Error]    At line " << ptr->lineNr << 
                " `" << ptr->tail << '\'' << endl;
    return 0;
}
