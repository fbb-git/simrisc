//#define XERR
#include "scenario.ih"

void Scenario::writeParameters(ostream &out, size_t iter) const
{
    out <<
        "Scenario:\n"
        "  iteration:    " << iter << "\n"
        "  generator:    " << s_generatorType[d_generatorType] << "\n"
        "  n iterations: " << d_nIterations << "\n"
        "  n cases:      " << d_nCases << "\n"
        "  seed          " << d_seed << "\n"
        "  spread:       " << s_bool[Globals::vary()] << '\n';

    if (d_cumDeathProportions.size() != 0)
    {
        out << "  death:";

        size_t idx = 0;         
        for (double prop: d_cumDeathProportions)
        {
            if (idx++ % 10 == 0)
                out << '\n' <<
                        setw(7) << idx << ':';
            out << ' ' << prop;
        }
        if (idx % 10 != 0)
            out.put('\n');
    }

    out.put('\n');
}
