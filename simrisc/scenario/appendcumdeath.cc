//#define XERR
#include "scenario.ih"

bool Scenario::appendCumDeath(string const &line)
{
    istringstream in{ line };

    char ch;
    size_t nr;

    if
    (
        not (in >> nr >> ch)
        or nr != (d_cumDeathProportions.size() + 1) or
        ch != ':'
    )
        return false;

    double value;
    while (in >> value)
        d_cumDeathProportions.push_back(value);

    return true;
}
