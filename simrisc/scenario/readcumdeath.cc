//#define XERR
#include "scenario.ih"

void Scenario::readCumDeath(string const &fname)
{
    ifstream in = Exception::factory<ifstream>(fname);
    string line;

    size_t lineNr = 0;

    while (getline(in, line))
    {
        ++lineNr;
        if
        (
            size_t pos = line.find_first_not_of(" \t"); // ignore empty and
            pos == string::npos or line[pos] == '#'     // comment lines
        )
            continue;

        if (not appendCumDeath(line))
        {
            Err::msg(Err::CUM_DEATH) <<  fname << " line " << lineNr <<
                        "): format error in `" << line << '\'' << endl;
            return;
        }
    }
}
