//#define XERR
#include "scenario.ih"

void Scenario::checkCumDeath(string const &source,
                             LineInfo const *previous) const
{
    if (size_t size = d_cumDeathProportions.size(); size != END_AGE)
    {
        previous = showLineInfo(previous);
        Err::msg(Err::CUM_DEATH) <<  source << "): " <<
                                    END_AGE << " proportions required, " <<
                                    size << " specified" << endl;
    }

    for (auto begin = d_cumDeathProportions.begin(),
         iter = begin, next = iter + 1, end = d_cumDeathProportions.end();
            next < end;
                ++iter, ++next)
    {
        if (*next < *iter)
        {
            previous = showLineInfo(previous);
            Err::msg(Err::CUM_DEATH) << source << ") proportions decrease: " <<
                    (next - begin) << " (" << *next << ") less than " <<
                    (iter - begin) << " (" << *iter << ')' << endl;
        }
    }
}
