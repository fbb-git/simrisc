#define XERR
#include "beir7.ih"

//#Tumor:
//    Beir7:
//            #       eta     beta    spread  dist.
//        breast:     -2.0    0.51    0.32    Normal
//

//#Beir7
//                                      Read by BetaDist (./betadist)
//                             >>> dist: not used unless spread == true
//        # LC:    eta   beta  dist    constant factor   aParam    bParam
//        male:    -1.4   .32  Beta    .234091  1.72727  2.664237  5.184883
//        female:  -1.4  1.40  Beta    .744828   .818966 3.366115  4.813548

Beir7::Beir7()
:
    d_vsd(VARY_NONNEG)
{
    switch (Globals::simulationType())
    {
        case BREAST:
            Parser::extract( Parser::one({ "Beir7:", "breast:" }),
                            d_eta, d_vsd);
        break;

        case MALE:
            Parser::extract( Parser::one({ "Beir7:", "male:" }),
                            d_eta, d_vsd);
        break;

        case FEMALE:
            Parser::extract( Parser::one({ "Beir7:", "female:" }),
                            d_eta, d_vsd);
        break;
    }
}

