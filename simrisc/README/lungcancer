Modified to implement lungcancer simulations in simrisc, according to the
'Adaptations in SIMRISc for modelling lung cancer screening' page:

1. Sensitivity computations:
----------------------------

    Survival loads S3 and S4. Survival's operator[] returns de VSD row to use
    given the tumor's diameter, set by 'Survival::setVSD'

    Survival::setVSD is called from Tumor:characteristics, called from
    loop:
        characteristics.cc
        intervalcancer.cc
        leaving.cc
        postscreen.cc
        pretumordeath.cc
        screen.cc

At screening ages < detectable age the diameter remains 0, thereafter it is
determined. E.g.,

    age: 50, detectable age: 54.4583, diameter = 0
    age: 52, detectable age: 54.4583, diameter = 0
    age: 55.3796, detectable age: 54.4583, diameter = 9.8397


    With 1000 cases 141 instances of a tumor diameter != 0 are generated.

2. Survival computations:
-------------------------

    Function Q uses the a & b values, function Z the c & d values.
    called from function F, called by tumor::setdeathAge

    E.g., for
           lung0: a        .00143      .00095      Normal
           lung0: b       1.84559      .33748      Normal
           lung0: c       -.22794      .07823      Normal
           lung0: d       1.06799      .16226      Normal

    tumor/functionf.cc produces: a..d: 0.00143 1.84559 -0.22794 1.06799

3. (Treatment) costs:
---------------------

    Configured as
        #        screening    diagnosis    M0      M1   (M0, M1: Table S3)
        costs:      176         1908      37909   56556

    Treatment costs: M0 en M1 costs are used for cases without and with
                     metastases.

    At each screening round the 'screening' costs are added, at each
    maybeFalsePositive test the diagnosis costs is added.

4. Radiation risk:
------------------

    Computed at the case initialization: the dose values are configured in the
    configuration file:
    CT:
        dose:  1

    which value is used when computing the screening radiation risk, using the
    unmodified computations based on errRisk1405 and beir7Err

------------------------------------------------------------------------------
The following classes perform different actions for breast- and lungcancer
simulations:

        Costs
        Modalities
        ProbGroup
        Screening
        Survival

They are all (directly or indirectly) used by Loop::iterate.

------------------------------------------------------------------------------
Costs:
    costs1.cc sets the variables specific for lungcancer
        (cf. dot.config/simrisc)
        for LC: fixed using (d_using), screening (lcScreening),
                (non)metastasis fixed treatment costs,
        for BC: age-based/usingCosts discount (otherUsing), age-based/cost
                discount (otherScreening), age/diameter based discount
                (otherTreatment).

------------------------------------------------------------------------------
Modalities:
    with LC only the CT Modality is loaded from the configuration file,
    with BC Mammo, Tomo and MRI.

    For each modality its costs, sensitivity and specificity are loaded
    from the configuration file, Except for MRI dose-values are loaded, with
    Mammo systematicError, beta and m-values are configured.


------------------------------------------------------------------------------
ProbGroup:
    only labels for LC/BC simulation are different, which are used when
    inserting probabilities in output files.

------------------------------------------------------------------------------
Screening:
    when LC simulation is requested and at a round specification (Screening:
    round: ...)  a BC modality is specified an error is issued. Likewise, when
    BC simulation is requested and the CT modality is specified an error is
    issued.

------------------------------------------------------------------------------
Survival:
    with LC simulation the S3 and S4 matrices are loaded from the
    config. file. In that case, once a tumor diameter is available the S4
    column to use can be computed (setVSD() calls cptVSDrow via
    d_cptVSDrow). This provides the a..d parameter values for the Z and Q
    functions (defined in the Tumor class).
    with BC simulation the a..d parameters are defined in the configuration
    file at 'Survival: type:' and these values are stored in the 1st row of
    the d_vsdMatrix.
    The index operator then produces the active a..d values using indices
    0..3. With LC simulations: a metastasis is observed when d_vsdRow >= 2,
    corresponding to columns 3 and 4 of table s4.

------------------------------------------------------------------------------
Loop:
    see loop/README

// Loop:
// =====
//
// iterate() iterates over the number of scenario iterations.
//     - it calls genCases() to generate the 'nCases' simulated cases.
//
// genCases() calls, for each of the 'nCases':
//     - caseInit()
//     - preScreen()
//     - screening()
//     - postScreen()
//
// caseInit() initializes the data of a simulated case.
//     - d_biRadIndices is a vector of probabilities initialized with randomly
//                     determined values:
//                    for breast cancer simulations: bi-rad indices for the ages
//                     of the screening rounds,
//                    for lung cancer simulations: randomly determined indices in
//                     the rows of table S3.
//     - d_tumorInfo's cumulative total risk is determined from
//                     Screening::radiationRisk
//
// preScreen() may result in a self-detected tumor before the screening. In that
//                     case the cost is added to the treatment cost.
//                     Costs::treatment doesn't use diameter with lung cancer and
//                     doesn't use metastasis with breast cancer simulations.
//
//     preScreen() may call 'selfDetected()' which is
//
//                 'characteristics(LEFT_PRE, TUMOR_PRE)', adding
//                 'treatmentCosts(d_tumor.arge())' to
//                 d_castCost. 'treatmentCosts' returns
//                     d_costs.treatment(screeningAge,
//                              d_tumor.diameter(), d_tumor.metastasis())
//                     (metastasis used with lung cancer,
//                      diameter used otherwise), see Costs.
//
// If the case still exists following preScreen(), screening() and postScreen()
// are called:
//
// screening() iterates over the screening rounds for as long as the case remains
//                     present (which may be over the full number of screening
//                     rounds).
//                    The case may leave (leaving()) at an interval cancer or
//                     because of natural death.
//                    If the case doesn't prematurely leave, 'screen()' is called
//                     which may result in (maybe false positive) tumor detection
//
//         leaving(): may call
//             intervalCancer(): adds the costs via treatmentCosts, and the case
//                     leaves the simulation and may leave because of natural
//                     death.
//
//         screen():
//             visits all modalities, but with LC that's the only allowed
//                 modality when specifying the rounds at Screening: in the
//                 .config/simrisc file.
//
//             maybeDetect:        calls treatmentCosts.
//             maybeFalsePositive: adds costs via d_costs.usingAt(), determining
//                                     the costs via its d_using pointer.
//
// If the case is present following 'screening()' postScreen() is called:
//
// postScreen()
//     adds costs (if a tumor was detected before death),
//         via treatmentDeath: characteristics(LEFT_POST, TUMOR_POST)




------------------------------------------------------------------------------
Lung cancer tumor computations
==============================

Tumor calculations use three functions:

'functionF' returns, given the number of years:

    exp(-functionQ(years) * pow(d_diameter, functionZ(years)));

These parameters are also used for F(D) on p. 6 of 'supplementary.odt':

            F(D) = e^-QD^Z

Here F(D) D is the tumor diameter, and Z is the function Z defined as
Z(t) = c ln(t) + d.

For lung cancer simulations Z can also remain as-is, as its 'c' and 'd'
parameters are the survival values [2] and [3], which, for lung-cancer, are
computed as described in supplementary.odt's tables S3 and S4:

    double Tumor::functionZ(double years) const
    {
        return d_survival[2].value() * log(years)
               + d_survival[3].value();
    }


Likewise, functionQ can remain as-is, as its 'a' and 'b' parameters are the
survival parameters [0] and [1], which, for lung-cancer, are also
computed as described in supplementary.odt's tables S3 and S4:

    double Tumor::functionQ(double years) const
    {
        return d_survival[0].value() * pow(years, d_survival[1].value());
    }

Consequently, the Survival object instead of the Tumor object is adapted
for lung-cancer simulations.

--------------------------------------------------------------------------

For lung cancer simulations the tumor size determines the Survival's a..d
([0]..[3]) values. For breast cancer simulations the Survival's a..d values
are known once the tumor's diameter is known.

The TumorInfo object constructs the Survival object, which defines the a..d
parameter values either for the breast cancer simulations or for the
lung-cancer simulations.

// The Survival class provides the member computeAD which does nothing for
// breast cancer computations (as breast cancer computations use fixed a-d
// parameters, read from the configuarion file) and is called by the Tumor object
// once the tumor's diameter has been determined (by
// Tumor::characteristics(double age)).
