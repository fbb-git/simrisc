'selfDetectable a tumor' means that Tumor has determined an age (d_age !=
NO_TUMOR)

cptIndices: for BC: the bi-rad column index to use given the case's age,
            for LC: 0 vector, since the LC dose value is a fixed value in the
                    configuration file.
iterate()
    iterates over d_scenario.nIterations.
    calls genCases(), simulating the cases.

genCases()
    iterates over nCases. Per case:

    caseInit:
        initialize case variables like costs, detected etc.
        The case's status is set to PRESENT
        tumorInfo.cumTotalRisk: computes over all modalities of
                                all screening ages.
            calls Screening::radiationRisk,
            calls errRisk1405 for each screening round age, passing it
                indices[roundIndex] as the
            calls beir7Err for all subsequent ages, updating those
                radiationRisk vector elements

        tumor.reset: resets the tumor-specific variables, determines
            * if a tumor will occur,
            * the age at which the tumor will occur (tumorAge).

            At this point the tumor diameter has not yet been determined.
            The diameter is determined by tumor.characteristics,
            d_tumor.selfDetectable() returns true if there is an age where a
            tumor develops (tumor.d_age != NO_TUMOR)

    preScreen:
        if there's selfDetectable a tumor prescreening is performed:
            selfDetectable a pre-tumor natural death or a selfDetected
            tumor. If so,
                --> tumor.characteristics() is called, the tumor's
                    diameter is computed and the case leaves the
                    simulation
        the status remains PRESENT
        or it becomes
            LEFT_PRE at the case's natural death age, or
            TUMOR_PRE at the tumor induced age of death

    screening:
        while the case's status is PRESENT, all screening rounds are
        performed.
        The case may LEAVE with status LEFT_DURING at
            * the case's natural death age
            * or when an interval cancer is detected
                (calls tumor.characteristics)
            * or because of a tumor induced death
                (status TUMOR_DURING at the tumor's death age).
        Otherwise a screening is performed by 'screen':
            for all the modalities specified at 'Screening: round:'
                * adds the costs,
                * if a tumor selfDetectable is present: computes the
                  tumor characteristics and calls selfDetectableDetect:
                    * may result in the case leaving the simulation with
                      status
                        * LEFT_DURING (at the natural death age)
                        * or TUMOR_DURING at the tumor caused death age)
                        * or calls selfDetectableFalsePositive
                            (only updates statistics).

    postScreen:
        * if a tumor is present at an age before the case's natural
          death age then the case's simulation ends (status TUMOR_POST),
          determining the tumor's characteristics (i.e., diameter) and
          updating the case's cost with the treatment costs.
        * If not, then the case leaves the simulation at the case's natural
          death age (status TUMOR_POST).
          There may stil be a tumor, in which case its characteristics at the
          case's death age are determined.
