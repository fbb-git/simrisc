//#define XERR
#include "random.ih"

double Random::logNormal(double mean, double stdDev)
{
    d_logNormal.param(LogNormalParams{ mean, stdDev });

    double ret = d_logNormal(d_engine[LOGNORMAL]);
    g_log('L') << "          logNormal random value: " << ret << nl;
    return ret;
}
