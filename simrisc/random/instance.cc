//#define XERR
#include "random.ih"

// static
Random &Random::instance()
{
    if (not s_random)
        throw Exception{} << "Random::instance: Random not yet initiaized";

    return *s_random;
}
