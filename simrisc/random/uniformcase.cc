#define XERR
#include "random.ih"

double Random::uniformCase()
{
    double ret = d_uniformCase(d_engine[UNIFORM_CASE]);
    g_log('C') << "          uniform case random value: " << ret << nl;
    return ret;
}
