//#define XERR
#include "random.ih"

double Random::binomial(double percentage)
{
    d_binomial->param(BinomialParams{ N_TRIALS, percentage });

    return (*d_binomial)(d_engine[BINOMIAL]) / static_cast<double>(N_TRIALS);
}
