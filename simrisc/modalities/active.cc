#define XERR
#include "modalities.ih"

void Modalities::active()
{
    for (string const &modality:            // use sorted modality names
                            set<string>{ d_active.begin(), d_active.end() })
        d_activeVect.push_back(d_modBaseVect[ find(modality) ]);
}
