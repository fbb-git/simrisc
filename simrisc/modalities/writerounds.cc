#define XERR
#include "modalities.ih"

void Modalities::writeRounds(CSVTable &tab, size_t round) const
{
    for (ModBase const *modBase: d_activeVect)           // insert the values
        tab.more() << (*modBase)[round];
}
