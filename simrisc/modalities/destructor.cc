//#define XERR
#include "modalities.ih"

Modalities::~Modalities()
{
    for (auto modBasePtr: d_modBaseVect)
        delete modBasePtr;
}
