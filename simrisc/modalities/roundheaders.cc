//#define XERR
#include "modalities.ih"

void Modalities::roundHeaders(CSVTable &tab) const
{
    for (ModBase const *modBase: d_activeVect)           // insert the IDs
        tab.more() << modBase->id();
}
