inline StringVect const &Modalities::modalityIDs() const
{
    return d_modalityIDs;
}

inline void Modalities::activate(std::string const &modality)
{
    d_active.emplace(modality);
}
