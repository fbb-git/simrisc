//#define XERR
#include "modalities.ih"

void Modalities::writeParameters(ostream &out) const
{
    out << "Modalities:\n"
           "\n";

    for (auto modBasePtr: d_modBaseVect)
        out << *modBasePtr;
}
