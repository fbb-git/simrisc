//#define XERR
#include "mammo.ih"

void Mammo::setSystematicError()
{
    base().back() = "systematicError:";
    Parser::proportion(base(), d_sysErr);
}
