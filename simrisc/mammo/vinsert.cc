#define XERR
#include "mammo.ih"

void Mammo::vInsert(ostream &out) const
{
    //Globals::setPrecision(out, 3) <<
    Globals::setWidthPrec(out, 4, 3) <<
                setw(4) << ' ' << "systematicError:  " << d_sysErr << '\n';

    VSD::fmt(6, "bi-rad:", 'a', 1, 0, 1, 0);
    out << setw(4) << ' ' << "Dose:\n" <<
           d_dose;

    VSD::fmt(6, "bi-rad:", 'a', 0, 3, 0, 3);
    out << setw(4) << ' ' << "M:\n" <<
           d_m;

    VSD::fmt(6, "nr:", 0, 3, 2, 0, 4);
    out << setw(4) << ' ' << "Beta:\n" <<
           d_beta;

    AgeGroupVSD::fmt(6, 0, 3, 0, 3);
    out << setw(4) << ' ' << "Specificity:\n" <<
           d_specVect;
}
